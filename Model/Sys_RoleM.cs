﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    using DbFrame.Class;
    [Table("Sys_Role")]
    /// <summary>
    ///  角色 Role_ID, Role_Num, Role_Name, Role_Remark, Role_CreateTime
    /// </summary>
    public class Sys_RoleM : BaseEntity<Sys_RoleM>
    {
        [Field("ID",IsPrimaryKey =true)]
        public Guid Role_ID { get; set; }
        [CRequired(ErrorMessage = "{name}不能为空")]
        [CRepeat(ErrorMessage = "{name}不能重复")]
        [Field("编号")]
        public string Role_Num { get; set; }
        [CRepeat(ErrorMessage = "{name}不能为空")]
        [CRequired(ErrorMessage = "{name}不能为空")]
        [Field("角色名称")]
        public string Role_Name { get; set; }
        [Field("说明")]
        public string Role_Remark { get; set; }
        /// <summary>
        ///  1:是  2否
        /// </summary>
        [Field("是否可以删除?")]
        public int? Role_IsDelete { get; set; }
        [Field("创建时间")]
        public DateTime? Role_CreateTime { get; set; }
    }
}
