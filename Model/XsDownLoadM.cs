using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	//
	using DbFrame.Class;

	[Table("XsDownLoad")]
    public class XsDownLoadM : BaseEntity<XsDownLoadM>
    {
		[Field("Guid", IsPrimaryKey = true)]
		public Guid XsDownLoad_ID { get; set; }

		[Field("机器编号")]
		public string XsDownLoad_MachineName { get; set; }

		[Field("上传文件路径")]
		public string XsDownLoad_MachineUrl { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? XsDownLoad__CreateTime { get; set; }


    }
}
