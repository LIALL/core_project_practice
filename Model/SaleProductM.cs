using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	//产品销售统计表
	using DbFrame.Class;
	[Table("SaleProduct")]
    public class SaleProductM : BaseEntity<SaleProductM>
    {
		[Field("SaleProduct_ID", IsPrimaryKey = true)]
		public Guid SaleProduct_ID { get; set; }

		[Field("销售公司/单位")]
		public string SaleProduct_OwerComanyName { get; set; }

		[Field("所销售省份")]
		public string SaleProduct_Province { get; set; }

		[Field("所销售城市")]
		public string SaleProduct_City { get; set; }

		[Field("联系人和电话")]
		public string SaleProduct_LinkMan { get; set; }

		[Field("Sfy数量")]
		public string SaleProduct_SfyNum { get; set; }

		[Field("Sfy备注")]
		public string SaleProduct_SfyPzRemark { get; set; }

		[Field("Sxy数量")]
		public string SaleProduct_SxyNum { get; set; }

		[Field("发货时间")]
		public DateTime? SaleProduct_SaleDatetime { get; set; }
		[Field("己方负责人")]
		public string SaleProduct_PrincipalMan { get; set; }

		[Field("备注")]
		public string SaleProduct_AllRemark { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? SaleProduct__CreateTime { get; set; }


    }
}
