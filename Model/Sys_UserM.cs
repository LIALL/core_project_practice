﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
   using DbFrame.Class;
    /// <summary>
    ///  用户 User_ID, User_Name, User_LoginName, User_Pwd, User_Email, User_Token, User_CreateTime
    /// </summary>
    [Table("Sys_User")]
    public class Sys_UserM : BaseEntity<Sys_UserM>
    {

        [Field("ID", IsPrimaryKey = true)]
        public Guid User_ID { get; set; }
        [CRequired(ErrorMessage = "用户名不能为空")]
        [Field("用户名称")]
        public string User_Name { get; set; }
        [CRequired(ErrorMessage = "登录名不能为空")]
        [CRepeat(ErrorMessage = "登录名不能重复")]
        [Field("登录名")]
        public string User_LoginName { get; set; }
        [CRequired(ErrorMessage = "密码不能为空")]
        [Field("登录密码")]
     
        public string User_Pwd { get; set; }
        [Field("邮件")]
        [CRequired(ErrorMessage = "邮件不能为空")]
        public string User_Email { get; set; }
        
        [Field("是否可删除")]
        /// <summary>
        /// 1:是  2 否
        /// </summary>
        public int? User_IsDelete { get; set; }
        [Field("创建时间",IsIgnore =true)]
        public DateTime? User_CreateTime { get; set; }


    }
}
