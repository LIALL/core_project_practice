using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{

	//
	using DbFrame.Class;

	[Table("Sys_AppLog")]
    public class Sys_AppLogM : BaseEntity<Sys_AppLogM>
    {
		[Field("AppLog_ID", IsPrimaryKey = true)]
		public Guid AppLog_ID { get; set; }

		[Field("请求路径")]
		public string AppLog_Api { get; set; }

		[Field("请求_IP")]
		public string AppLog_IP { get; set; }

		[Field("AppLog_UserID")]
		public Guid? AppLog_UserID { get; set; }

		[Field("AppLog_Parameter")]
		public string AppLog_Parameter { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? AppLog_CreateTime { get; set; }


    }
}
