using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	//
	using DbFrame.Class;

	[Table("Network_Home")]
    public class Network_HomeM : BaseEntity<Network_HomeM>
    {
		[Field("主键", IsPrimaryKey = true)]
		public Guid Home_ID { get; set; }
        [Field("首页- 标题")]
        public string Home_Conpany_Title { get; set; }
        [Field("首页轮转图片1")]
		public string Home_RotateImageURL { get; set; }
        [Field("首页轮转图片2")]
        public string Home_RotateImageURLOne { get; set; }
        [Field("首页轮转图片3")]
        public string Home_RotateImageURLTwo { get; set; }
        [Field("首页轮转图片4")]
        public string Home_RotateImageURLThree { get; set; }
        [Field("菜单--ID")]
		public string Home_Contacttype { get; set; }

		[Field("首页- 底部编辑")]
		public string Home_ContentFonter { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Home_CreateTime { get; set; }


    }
}
