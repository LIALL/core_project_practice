using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	//
	using DbFrame.Class;

	[Table("Network_Product")]
    public class Network_ProductM : BaseEntity<Network_ProductM>
    {
		[Field("编号", IsPrimaryKey = true)]
		public Guid Product_ID { get; set; }
        [CRequired(ErrorMessage = "产品名称不能为空")]
        [CRepeat(ErrorMessage = "产品名称不能重复")]
        [Field("产品名称")]
        public string Product_Name { get; set; }
        
		

		[Field("产品特色")]
		public string Product_Feature { get; set; }

		[Field("产品功能")]
		public string Product_Funtion { get; set; }

		[Field("创新点")]
		public string Product_News { get; set; }

		[Field("产品图片")]
		public string Product_Image { get; set; }
        [CRepeat(ErrorMessage = "产品_MenuID不能重复")]
        [CRequired(ErrorMessage ="产品_MenuID不能为空")]
		[Field("产品_MenuID")]
		public string Product_MenuID { get; set; }
        [Field("产品详情介绍")]
        public string Product_Introduce { get; set; }
        [Field("创建时间", IsIgnore = true)]
		public DateTime? Product_CreateTime { get; set; }


    }
}
