using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	//
	using DbFrame.Class;

	[Table("Network_NewsCenter")]
    public class Network_NewsCenterM : BaseEntity<Network_NewsCenterM>
    {
		[Field("NewsCenter_ID", IsPrimaryKey = true)]
		public Guid NewsCenter_ID { get; set; }

		[Field("新闻标题")]
		public string NewsCenter_title { get; set; }

		[Field("新闻摘要")]
		public string NewsCenter_Abstract { get; set; }

		[Field("新闻内容")]
		public string NewsCenter_Content { get; set; }

		[Field("是否指定")]
		public string NewsCenter_IsTop { get; set; }

		[Field("录入人")]
		public string NewsCenter_lrr { get; set; }

		[Field("MenuID")]
		public string NewsCenter_MenuID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? NewsCenter_CreateTime { get; set; }


    }
}
