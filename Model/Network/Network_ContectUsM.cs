using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	//
	using DbFrame.Class;

	[Table("Network_ContectUs")]
    public class Network_ContectUsM : BaseEntity<Network_ContectUsM>
    {
		[Field("ContectUs_ID", IsPrimaryKey = true)]
		public Guid ContectUs_ID { get; set; }

		[Field("内容")]
		public string ContectUs_Content { get; set; }

		[Field("录入人")]
		public string ContectUs_Lrr { get; set; }

		[Field("创建时间",IsIgnore =true)]
		public DateTime? Recruitment_Createtime { get; set; }


    }
}
