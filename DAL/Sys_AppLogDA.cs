using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace DAL
{
	//
	using Common;
	using DbFrame;
	using DbFrame.Class;
	using DAL.Class;
	using System.Collections;
    using Model;
    using Microsoft.AspNetCore.Http;

    public class Sys_AppLogDA : BaseDAL
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
            var IQuery = Db
                .Query<Sys_AppLogM, Sys_UserM>((a, b) => new { a.AppLog_Api, a.AppLog_ID, a.AppLog_IP, b.User_LoginName, a.AppLog_CreateTime })
                .LeftJoin((a, b) => a.AppLog_UserID == b.User_ID, "b");

            return this.FindPaging(IQuery, page, rows,new Sys_AppLogM(),new Sys_UserM());

        }

        public static void InsertAppLog(HttpContext _httpContext, Guid Userid)
        {

            var _QueryString = _httpContext.Request.QueryString.ToString();

            var _ApiUrl = _httpContext.Request.Path.ToString() + _QueryString;
            var _Ip = _httpContext.Connection.RemoteIpAddress.ToString();
            var _FormJson = string.Empty;
            var _Mehton = _httpContext.Request.Method;
            //if (_IP == "::1") return;
          
                try
                {
                    var _Form = _httpContext.Request.Form;
                    var _Dictionary = new Dictionary<string, object>();
                    foreach (var key in _Form.Keys)
                    {

                        _Dictionary[key] = _Form[key];
                    }
                    _FormJson = JsonConvert.SerializeObject(_Dictionary);

                    Db.Add(new Sys_AppLogM
                    {
                        AppLog_Api = _ApiUrl,
                        AppLog_IP = _Ip,
                        AppLog_Parameter = _FormJson,
                        AppLog_UserID = Userid
                       
                    });
                }
                catch (Exception )
                {
                   // throw new MessageBox(ex.ToDateTimeFormat());
                }
        }
    }
}
