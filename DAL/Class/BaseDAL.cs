﻿
namespace DAL.Class
{

    using System;
    using System.Collections.Generic;
    using System.Text;
    using DbFrame.Class;
    using DbFrame;
    using Common;
    using Model;
    using System.Reflection;
    using System.Data;
    using System.Linq;
    using DbFrame.SQLContext.Interface;

    public class BaseDAL
    {
        /// <summary>
        /// 登录 信息  对象
        /// </summary>
        protected Sys_AccountM Account = new Sys_AccountM();


      
       protected  static DBContext Db => new DBContext();
        public BaseDAL() {

            Account = this.GetSession<Sys_AccountM>("Account");

        }
        /// <summary>
        /// 获取Session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>

        public T GetSession<T>(string key)
        {
            return Tools.GetSession<T>(key);
        }
        /// <summary>
        /// 设置Sesson
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetSession<T>(string key,object value) {


             Tools.SetSession(key, value);
        }
        public Sys_PagingEntity NewPagingEntity(Sys_PagingEntity pe, params EntityClass[] ArryEntity)
        {
            var dic = new Dictionary<string, object>();
            var list = new List<PropertyInfo>();
            var colNames = new List<Dictionary<string, string>>();
            ArryEntity.ToList().ForEach(item =>
            {
                //将所有实体里面的属性放入list中
                ReflexHelper.GetPropertyInfos(item.GetType()).ToList().ForEach(p =>
                {
                    list.Add(p);
                });
            });
            foreach (DataColumn dc in pe.Table.Columns)
            {
                dic = new Dictionary<string, object>();
                var col = new Dictionary<string, string>();
                var pro = list.Find(item => item.Name.Equals(dc.ColumnName));

                dic["field"] = dc.ColumnName;
                dic["align"] = "left";
                if (pro == null)
                {
                    dic["title"] = dc.ColumnName;
                    dic["visible"] = !dc.ColumnName.Equals("_ukid");
                    col.Add(dc.ColumnName, dc.ColumnName);
                }
                else
                {
                    //获取有特性标记的属性【获取字段别名（中文名称）】
                    var FiledConfig = pro.GetCustomAttribute(typeof(FieldAttribute)) as FieldAttribute;
                    dic["title"] = (FiledConfig.Alias == "" ? dc.ColumnName : FiledConfig.Alias);
                    dic["visible"] = true;
                    col.Add(dc.ColumnName, dic["title"].ToStr());
                }
                pe.ColNames.Add(col);
                pe.ColModel.Add(dic);
            }

            return pe;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="SqlStr"></param>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Param"></param>
        /// <param name="ArryEntity"></param>
        /// <returns></returns>
        public Sys_PagingEntity FindPaging(string SqlStr, int Page, int Rows, object Param, params EntityClass[] ArryEntity)
        {
            var Total = 0;
            var _Sys_PagingEntity = new Sys_PagingEntity();
            var _FindPaging = Db.FindPaging(SqlStr, Page, Rows, Param);
            Total = _FindPaging.Total;
            _Sys_PagingEntity.Table = _FindPaging.Table;
            _Sys_PagingEntity.Counts = Total;
            _Sys_PagingEntity.PageCount = (Total / Rows);
            _Sys_PagingEntity.List = _FindPaging.Table.ToList();
            return this.NewPagingEntity(_Sys_PagingEntity, ArryEntity);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Iquery"></param>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="ArryEntity"></param>
        /// <returns></returns>
        public Sys_PagingEntity FindPaging(IQuery Iquery, int Page, int Rows, params EntityClass[] ArryEntity)
        {
            return this.FindPaging(Iquery.ToSQL(), Page, Rows, Iquery.GetSqlParamters(), ArryEntity);
        }
     
    }
}
