﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Linq;
namespace DAL
{
    using DbFrame.Class;
    using Model;
    using System.Data;
    using DAL.Class;
   public class Sys_MenuDA  :BaseDAL
    {

        public Sys_PagingEntity GetDatasource(Hashtable Query, int Page, int rows) {

            var IQuery= Db.Query<Sys_MenuM,Sys_MenuM>((a,b)=> new { a.Menu_Name,a.Menu_Url,父及菜单=b.Menu_Name,a.Menu_Num,
                a.Menu_Icon, a.Menu_CreateTime,
                _ukid = a.Menu_ID

            }).LeftJoin((a,b)=>a.Menu_ParentID ==b.Menu_ID,"b")
            .WhereIF(string.IsNullOrEmpty(Query["Menu_ID"].ToStr()),(a,b)=>a.Menu_ParentID ==null || a.Menu_ParentID== Guid.Parse("00000000-0000-0000-0000-000000000000") )
            .WhereIF(!string.IsNullOrEmpty(Query["Menu_ID"].ToStr()), (a, b) => a.Menu_ParentID == Query["Menu_ID"].ToGuid())
            .WhereIF(!string.IsNullOrEmpty(Query["Menu_Name"].ToStr()), (a, b) => a.Menu_Name.Contains(Query["Menu_Name"].ToStr()))
            .OrderBy((a, b) => new { a.Menu_Num });


            return this.FindPaging(IQuery, Page, rows, new Sys_MenuM());

        }
        /// <summary>
        /// 根据角色ID 获取菜单
        /// </summary>
        /// <returns></returns>
        public DataTable GetMenuByRoleID()
        {
            var sql = @"select * from Sys_Menu order by Menu_Num asc";
            
            if (!this.Account.IsSuperManger)
            {
                var _roleid = this.Account.RoleID.ToGuid();
                sql = @"
                                    select * from (

                                    select Menu_ID, a.Menu_Num, Menu_Name, Menu_Url, Menu_Icon, a.Menu_ParentID, Menu_CreateTime 
                                    from (select * from Sys_Menu where 1=1 and Menu_Url is null or Menu_Url='') a
                                     join (
	                                    select Menu_Num,Menu_ParentID
		                                    from [dbo].[Sys_RoleMenuFunction] 
		                                    join Sys_Menu on Menu_ID=RoleMenuFunction_MenuID and RoleMenuFunction_RoleID='" + _roleid + @"'
		                                    group by RoleMenuFunction_MenuID,RoleMenuFunction_RoleID,Menu_Num,Menu_ParentID
                                    ) b on charindex(a.Menu_Num,b.Menu_Num)>0 and a.Menu_ID=b.Menu_ParentID
                                    union
                                    select Menu_ID, Menu_Num, Menu_Name, Menu_Url, Menu_Icon, Menu_ParentID, Menu_CreateTime 
                                    from Sys_Menu x
                                    join (
	                                    select RoleMenuFunction_MenuID,RoleMenuFunction_RoleID 
		                                    from [dbo].[Sys_RoleMenuFunction] 
		                                    group by RoleMenuFunction_MenuID,RoleMenuFunction_RoleID
                                    ) y on x.Menu_ID=y.RoleMenuFunction_MenuID and y.RoleMenuFunction_RoleID='" + _roleid + @"'

                                    ) tab
                                    order by tab.Menu_Num asc
                                ";
            }
            return Db.FindTable(sql, null);
        }

        /// <summary>
        /// 获取菜单和功能树
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetMenuAndFunctionTree(string RoleID = "")
        {
            //菜单功能查询
            string sql = @"SELECT (Menu_Name+'('+Menu_Num+')') name,Menu_ID id,Menu_ParentID pId,Menu_Num num,Menu_Url ur,'false' checked,null tag,'true' chkDisabled,
case when  Menu_ParentID is not null then 'false' else 'true' end [open]
                                FROM Sys_Menu 
		                        ORDER BY Menu_Num";

            if (!RoleID.ToGuid().Equals(Guid.Empty))
            {
                //角色功能查询
                sql = @"SELECT (Menu_Name+'('+Menu_Num+')') name,Menu_ID id,Menu_ParentID pId,Menu_Num num,Menu_Url ur,'false' checked,null tag ,
case when  Menu_ParentID is not null then 'false' else 'true' end [open]
		                        FROM Sys_Menu A
		                        LEFT JOIN Sys_RoleMenuFunction B ON A.Menu_ID=B.RoleMenuFunction_MenuID
		                        WHERE 1=1 AND B.RoleMenuFunction_RoleID='" + RoleID.ToGuid() + @"'
		                        ORDER BY Menu_Num";
            }

            return Db.FindList<Dictionary<string, object>>(sql, null).ToList();
        }

    }
}
