using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
	//
	using Common;
	using DbFrame;
	using DbFrame.Class;
	using Model;
	using DAL.Class;
	using System.Collections;

    public class Network_ProductDA : BaseDAL
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
			var IQuery = Db
                .Query<Network_ProductM>((a) => new {a.Product_ID ,a.Product_Name,a.Product_Feature
                ,a.Product_Funtion,a.Product_News,a.Product_Image,a.Product_MenuID,_ukid=a.Product_ID});

            return this.FindPaging(IQuery, page, rows,new Network_ProductM());

        }


    }
}
