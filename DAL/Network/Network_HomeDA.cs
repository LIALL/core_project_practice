using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
	//
	using Common;
	using DbFrame;
	using DbFrame.Class;
	using Model;
	using DAL.Class;
	using System.Collections;

    public class Network_HomeDA : BaseDAL
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
			var IQuery = Db
                .Query<Network_HomeM, Sys_MenuM>((a,b) => new
                { _ukid=a.Home_ID ,a.Home_Contacttype,a.Home_Conpany_Title,a.Home_ContentFonter,a.Home_CreateTime})
                //.LeftJoin((a,b)=>a.Home_Contacttype==b.Menu_ID.ToStr(),"b")
                //.WhereIF(!string.IsNullOrEmpty(query["Conpany_Title"].ToStr()), (a, b) => a.Home_Conpany_Title.Contains(query["Home_Conpany_Title"].ToStr()))
                //.OrderBy((a, b) => new { desc = a.Home_CreateTime })
                ;

            return this.FindPaging(IQuery, page, rows,new Network_HomeM(),new Sys_MenuM());

        }


    }
}
