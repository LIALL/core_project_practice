﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers
{
    using BLL;
    using Common;
    using Model;
    public class HomeController : BaseController
    {

        Sys_MenuBL _Sys_MenuBL = new Sys_MenuBL();
        Sys_AccountM _Sys_AccountM = new Sys_AccountM();
        protected override void Init()
        {
            base.Init();
            this.IsExecutePowerLogic = false;
        }

        public override IActionResult Index()
        {
            _Sys_AccountM= Tools.GetSession<Sys_AccountM> ("Account");
            ViewData["Week"] =DateTime.Now;
            ViewData["UserName"] = _Sys_AccountM.UserName;
            ViewData["MenuHtml"] = _Sys_MenuBL.GetSysMenu();
            return View(Account);
        }

        public IActionResult Main()
        {
            return View();
        }
    }
}