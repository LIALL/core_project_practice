﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace AdminManagement.Areas.Admin.Controllers.Base
{
    using Microsoft.AspNetCore.Hosting;
    using BLL;
    using DbFrame.Class;
    using Model;
    using System.Collections;
    using Microsoft.AspNetCore.Http;
    public class MemberController : BaseController
    {
        private readonly IHostingEnvironment _HostingEnvironment;
        private readonly string _WebRootPath = string.Empty;
        protected override void Init()
        {
            // base.Init();
            this.MenuID = "A-100";
            this.PrintTitle = "Member   Print";
        }
        public MemberController(IHostingEnvironment hostingEnvironment) {

            this._HostingEnvironment = hostingEnvironment;
            _WebRootPath = this._HostingEnvironment.WebRootPath;
        }
        #region

        MemberBL _MemberBL = new MemberBL();
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _MemberBL.GetDataSource(query, page, rows);
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="_Member_formFile_Photo_One"></param>
        /// <param name="_Member_formFile__Two"></param>
        /// <returns></returns>
        [AOP.AopCheckEntityFilterAtttibute(new string[] { "model" })]
        [HttpPost]
        public ActionResult Save(MemberM model, IFormFile _Member_formFile_Photo_One, IFormFile _Member_formFile__Two) {

            if (_Member_formFile_Photo_One != null) {

                this.HandleUpFile(_Member_formFile_Photo_One, new string[] { ".jpg", ".png", ".gif" }, _WebRootPath, null, (_path) =>
                {

                    model.Member_Photo = _path;
                });
            }
            if (_Member_formFile__Two != null)
            {

                this.HandleUpFile(_Member_formFile__Two, new string[] { ".jpg", ".png", ".gif" }, _WebRootPath, null, (_path) =>
                {

                    model.Member_FilePath = _path;
                });
            }
            this.KeyID = _MemberBL.Save(model);
            return Json(new { status = 1, Id = KeyID });
        }
        [HttpPost]
        public ActionResult Find(string ID) {

            return Json(_MemberBL.Find(ID.ToGuid()));
        }
        [HttpPost]
        public ActionResult Delete(string ID) {

            _MemberBL.Delete(ID);
            return Json(new { status = 1 });
        }
        #endregion

    }

    
}