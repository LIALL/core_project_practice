﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//
using AdminManagement.AOP;
using Common;
namespace AdminManagement.Areas.Admin.Controllers
{
    public class ErrorController :BaseController
    {
        public IActionResult Index(ErrorModel _errorModel )
        {
            return View(_errorModel);
        }
    }
}