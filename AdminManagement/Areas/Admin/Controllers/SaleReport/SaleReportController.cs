﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers.SaleReport
{
    using AOP;
    using Microsoft.AspNetCore.Hosting;
    using BLL;
    using DbFrame.Class;
    using Model;
    using System.Collections;
    using Common;
    using Microsoft.AspNetCore.StaticFiles;
    using System.IO;
    using Microsoft.AspNetCore.Http;

    public class SaleReportController : BaseController
    {
        protected override void Init()
        {
            base.Init();
            this.MenuID = "C-100";
            this.PrintTitle = "产品销售记录打印";

        }
        SaleProductBL _saleProductBLL = new SaleProductBL();
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _saleProductBLL.GetDataSource(query, page, rows);
        }
        [AOP.AopCheckEntityFilterAtttibute(new string[] { "model" })]
        [HttpPost]
        public ActionResult Save(SaleProductM model) {

            this.KeyID = _saleProductBLL.Save(model);

            return Json(new { status = 1, ID = KeyID });
        }
        [HttpPost]
        public ActionResult Find(string ID) {

            return Json(_saleProductBLL.Find(ID.ToGuid()));

        }
        [HttpPost]
        public ActionResult Delete(string ID) {

              _saleProductBLL.Delete(ID);

            return Json(new {status = 1 });
                 
        }


    }
}