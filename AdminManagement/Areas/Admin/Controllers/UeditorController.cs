﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UEditor.Core;

namespace AdminManagement.Areas.Admin.Controllers
{
    [AOP.AopActionFilter(false)]
    public class UeditorController : BaseController
    {
        protected override void Init()
        {
            base.Init();
            this.IsExecutePowerLogic = false;
        }

        private readonly UEditorService _ueditorService;
        public UeditorController(UEditorService ueditorService)
        {
            this._ueditorService = ueditorService;
        }

        //如果是API，可以按MVC的方式特别指定一下API的URI
        [HttpGet, HttpPost]
        public ContentResult Upload()
        {
            var response = _ueditorService.UploadAndGetResponse(HttpContext);
            return Content(response.Result, response.ContentType);
        }
    }
}