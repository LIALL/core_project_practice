﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers
{
    using AOP;
    using global::Model;
    using DbFrame.Class;
    using Common;
    using Common.VerificationCode;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    [AopActionFilter(false)]
    public class LoginController : BaseController
    {
        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {
            this.IsExecutePowerLogic = false;
        }
        public override IActionResult Index()
        {

            Tools.SetSession("Account", new Sys_AccountM());
            return View();
        }
        //用户登录检查
        public IActionResult Checked(string uName, string uPwd, string loginCode) {

            //判断是否为空
            if (string.IsNullOrEmpty(uName))
                throw new MessageBox("请输入用户名！");
            if (string.IsNullOrEmpty(uPwd))
                throw new MessageBox("请输入密码！");
            if (string.IsNullOrEmpty(loginCode))
                throw new MessageBox("请输入验证码！");

            //是否和密码一致

            var _Sys_UserM = db.Find<Sys_UserM>(w => w.User_LoginName == uName);
            if (_Sys_UserM.User_ID.ToGuid() == Guid.Empty) {

                throw new MessageBox(" 账户不存在！");
            }
            if (_Sys_UserM.User_Pwd.Trim() != uPwd) {

                throw new MessageBox("密码错误,请重新输入！");
            }
            //验证码
            string code = Tools.GetCookie("loginCode");
            if (string.IsNullOrEmpty(code)) {

                throw new MessageBox("验证码失效");
            }
            if (!code.ToLower().Equals(loginCode.ToLower())) {

                throw new MessageBox("请输入正确的验证码！");
            }
            //用户角色信息
             
            var _Sys_UseRoleM = db.Find<Sys_UserRoleM>(w => w.UserRole_UserID == _Sys_UserM.User_ID);
            //角色信息
            var _Sys_RoleM = db.Find<Sys_RoleM>(w => w.Role_ID == _Sys_UseRoleM.UserRole_RoleID);

            var _Sys_Account = new Sys_AccountM();
            _Sys_Account.RoleID = _Sys_RoleM.Role_ID.ToGuid();
            _Sys_Account.UserID = _Sys_UserM.User_ID.ToGuid();
            _Sys_Account.UserName = _Sys_UserM.User_Name;
            _Sys_Account.IsSuperManger = _Sys_RoleM.Role_ID == AppConfig.Admin_RoleID.ToGuid();
            Tools.SetSession("Account", _Sys_Account);
            //保存账户信息
            //if (!string.IsNullOrEmpty(uName)) { throw new MessageBox("验证码正确1"); }
            return Json(new { status = 1, jumpurl = AppConfig.HomePageUrl });

        }
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        public IActionResult GetYZM()
        {
            var _Helper = new Helper();
            Tools.SetCookie("loginCode", _Helper.Text,2);
           // string code = Tools.GetCookie("loginCode");
            return File(_Helper.GetBytes(), Tools.GetFileContentType[".bmp"]);
        }
        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        public IActionResult Out()
        {
            return RedirectToAction("Index");
        }

      
    }
}