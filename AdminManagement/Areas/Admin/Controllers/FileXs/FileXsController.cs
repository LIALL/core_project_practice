﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;
using Microsoft.AspNetCore.Http;
namespace AdminManagement.Areas.Admin.Controllers.Xs
{

    using Microsoft.AspNetCore.Hosting;
    using BLL;
    using DbFrame.Class;
    using Model;
    using System.Collections;
    using Common;
    using AOP;
    using Microsoft.AspNetCore.StaticFiles;
    using System.IO;

    public class FileXsController :BaseController
    {
        public IHostingEnvironment _hostingEnvironment;
        public readonly string _WebRootPath=string.Empty;
        protected override void Init()
        {
            this.MenuID = "B-100";
            this.PrintTitle = "XSManage   Print";
        }
        public FileXsController(IHostingEnvironment hostingEnvironment) {
            _hostingEnvironment = hostingEnvironment;
            _WebRootPath = _hostingEnvironment.WebRootPath;
        } 
        XsDownLoadBLL _XsDownLoadBLL = new XsDownLoadBLL();
        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _XsDownLoadBLL.GetDataSource(query, page, rows);
        }
        //查找
        public ActionResult Find(string ID) {

            return Json(_XsDownLoadBLL.Find(ID.ToGuid()));
        }
        [AOP.AopCheckEntityFilterAtttibute(new string[] { "model"})]
        public ActionResult Save(XsDownLoadM model, IFormFile XsDownLoad_formFile) {
            //上传文件
            //if (XsDownLoad_formFile == null) {

            //    throw new MessageBox("请上传文件");
               
            //}
             //上传文件检查
            if (XsDownLoad_formFile != null)
            {
                this.HandleUpFile(XsDownLoad_formFile, new string[] { ".txt" }, _WebRootPath, null, (_path) =>
                {
                    model.XsDownLoad_MachineUrl = _path;

                });
               // throw new MessageBox("请上传文件");
            }
            this.KeyID = _XsDownLoadBLL.Save(model);
            //保存返回ID
            return Json(new { status = 1, ID = KeyID });
        }
        [HttpGet]
        /// <summary>
        /// 文件流的方式输出        /// </summary>
        /// <returns></returns>
        public IActionResult DownLoad(string ID)
        {
            XsDownLoadM _model= db.FindById<XsDownLoadM>(ID.ToGuid());
            var addrUrl = _WebRootPath+_model.XsDownLoad_MachineUrl.Replace("/","\\");
            var stream = System.IO.File.OpenRead(addrUrl);
            //获取文件的ContentType
            //var provider = new FileExtensionContentTypeProvider();
            //var memi = provider.Mappings[fileExt];
            return File(stream, "text/plain", Path.GetFileName(addrUrl));

        }
        [HttpPost]
        public ActionResult Delete(string ID)
        {

            _XsDownLoadBLL.Delete(ID);
            return Json(new { status = 1 });
        }


    }
}