﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace AdminManagement.Areas.Admin.Controllers.Sys
{
    using BLL;
    using DbFrame.Class;
    using Common;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    public class CreateCodeController : BaseController
    {
        private readonly Sys_CreateCodeBL _Sys_CreateCodeBL = new Sys_CreateCodeBL();
        public IHostingEnvironment _ihostingEnvironment = null;
        private string _WebRootPath = string.Empty;  
        public CreateCodeController(IHostingEnvironment ihostingEnvironment) {

            this._ihostingEnvironment = ihostingEnvironment;
            _WebRootPath = this._ihostingEnvironment.WebRootPath.ToString();
        }
 
        protected override void Init()
        {
            this.MenuID = "Z-160";
        }
        public override IActionResult Index()
        {
            ViewData["Path"] = (_WebRootPath+"\\Content\\CreateFile\\").Replace("\\","\\\\");
            return base.Index();
        }
        [HttpPost]
        public ActionResult GetDatabaseAllTable() {

            return Json(new { status = 1, value = _Sys_CreateCodeBL.GetDatabaseAllTable() });
                
        }
        /// <summary>
        /// 保存代码
        /// </summary>
        /// <param name="Fc"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Save(IFormCollection Fc) {

            var Type = Fc["ClassType"].ToStr() ;
            var Url=(Fc["Url"].ToStr()??_WebRootPath+"\\Content\\CreateFile\\");
            var Str = Fc["Str"];
            var Table = Fc["Table"];
            var isall = Fc["isall"].ToBool();
            var template = _WebRootPath + "\\Content\\Template\\";
            if (Type == "Model") {

                Url = (Url+"\\Model");
                template = template + "Model\\Model.txt";
                Str = string.IsNullOrEmpty(Str.ToStr()) ? "M" : Str.ToStr();
            }
            else if (Type == "BLL")
            {

                Url = (Url + "\\BLL");
                template = template + "BLL\\BLL.txt";
                Str = string.IsNullOrEmpty(Str.ToStr()) ? "BLL" : Str.ToStr();
            }
            else if (Type == "DLL")
            {

                Url = (Url + "\\DLL");
                template = template + "DLL\\DLL.txt";
                Str = string.IsNullOrEmpty(Str.ToStr()) ? "DA" : Str.ToStr();
            }
            if (System.IO.Directory.Exists(Url + "\\")) {

                var dir = new System.IO.DirectoryInfo(Url + "\\");
                var fileinfo = dir.GetFileSystemInfos();//返回目录中所有文件和子目录
                foreach (var i in fileinfo) {
                    if (i is System.IO.DirectoryInfo)
                    {
                        var subdir = new System.IO.DirectoryInfo(i.FullName);
                        subdir.Delete(true);//删除子目录和文件
                    }
                    else {

                        System.IO.File.Delete(i.FullName);     
                    }
                }
            }
            System.IO.Directory.CreateDirectory(Url);
            if (!System.IO.File.Exists(template))
                throw new MessageBox("模板文件不存在！");
            var Content = System.IO.File.ReadAllText(template);
            if (isall)
            {
                var list = _Sys_CreateCodeBL.GetAllTable();
                foreach (var item in list)
                {
                    Table = item["TABLE_NAME"] == null ? "" : item["TABLE_NAME"].ToStr();
                    this._Sys_CreateCodeBL.CreateFileLogic(Content, Table, Str, Type, Url);
                }
            }
            else {
                if (string.IsNullOrEmpty(Table)) {

                    throw new MessageBox("请选择表");  
                }
                this._Sys_CreateCodeBL.CreateFileLogic(Content, Table, Str, Type, Url);

            }
            return Json(new { status="1"});
        }
    }
}