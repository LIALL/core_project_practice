﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace AdminManagement.Areas.Admin.Controllers.Sys
{

    using global::Model;
    using BLL;
    using Newtonsoft.Json;
    using DbFrame.Class;
    using System.Collections;
    using Microsoft.AspNetCore.Http;
    using AOP;
    using Common;

    public class AppLogController : BaseController
    {
        Sys_AppLogBL _AppLogBL = new Sys_AppLogBL();

        protected override void Init() {


            this.MenuID = "Z-170";
        }

        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="page">PageIndex</param>
        /// <param name="rows">PageSize</param>
        /// <returns></returns>
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _AppLogBL.GetDataSource(query, page, rows);
        }
        public IActionResult Find(string ID) {


            return Json(_AppLogBL.Find(ID.ToGuid()));
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}

    }
}