﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers.Sys

{
    using global::Model;
    using BLL;
    using DbFrame.Class;
    using Common;
    using System.Collections;
    using AOP;
    using Microsoft.Extensions.Configuration;

    public class FunctionController : BaseController
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        Sys_FunctionBL _Sys_FunctionBL = new Sys_FunctionBL();
        protected override void Init() {

          
            this.MenuID = "Z-120";
        }
        /// <summary>
        /// 分页数据
        /// </summary>
        /// <param name="query"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {

            return _Sys_FunctionBL.GetDataSource(query, page, rows);
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AopCheckEntityFilterAtttibute(new string[] { ("model") })]
        [HttpPost]
        public IActionResult Save(Sys_FunctionM model) {

            this.KeyID = _Sys_FunctionBL.Save(model);
            
            return Json(new { status = 1, ID = KeyID });

        }
        [HttpPost]
        public IActionResult Find(string ID)
        {

          return Json( _Sys_FunctionBL.Find(ID.ToGuid()));
           
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Delete(string ID) {

            _Sys_FunctionBL.Delete(ID);

            return Json(new { status = 1 });
        }

       


    }
}