﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers.Sys
{
    using global::Model;
    using BLL;
    using DbFrame.Class;
    using Common;
    using System.Collections;

    public class RoleController : BaseController
    {
        //public IActionResult Index()
        Sys_RoleBL _Sys_RoleBL = new Sys_RoleBL();

        protected override void Init()
        {
            //base.Init();
            this.MenuID = "Z-110";
        }
        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _Sys_RoleBL.GetDataSource(query, page, rows);
        }
        /// <summary>
        /// 根据ID 获取对象
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Find(string ID) {

            
            return Json(_Sys_RoleBL.Find(ID.ToGuid()));

        }
       /// <summary>
       /// 删除
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        [HttpPost]
        public IActionResult Delete(string id) {

            _Sys_RoleBL.Delete(id);

            return Json(new { status=1 });
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AOP.AopCheckEntityFilterAtttibute(new string[] { "model"})]
        public IActionResult Save(Sys_RoleM model )
        {
            this.KeyID = _Sys_RoleBL.Save(model);
            return Json(new { status=1, ID=KeyID });
        }
        //{
        //    return View();
        //}







    }
}