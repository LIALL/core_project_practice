﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers.Sys
{
    using BLL;
    public class RoleFunctionController : BaseController
    {
        protected override void Init()
        {
            this.MenuID = "Z-140";
        }
        Sys_RoleBL _Sys_RoleBL = new Sys_RoleBL();
        Sys_MenuBL _Sys_MenuBL = new Sys_MenuBL();

        /// <summary>
        /// 获取角色菜单功能
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetRoleMenuFunctionTree(string roleid)
        {
            return Json(new { status = 1, value = _Sys_MenuBL.GetRoleMenuFunctionTree(roleid) });
        }
        public IActionResult Save(string rows,string roleid) {

            _Sys_RoleBL.SaveFunction(rows, roleid);

            return Json(new { status = 1 });
        }

    }
}