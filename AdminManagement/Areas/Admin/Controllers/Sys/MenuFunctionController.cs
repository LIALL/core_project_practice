﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers.Sys
{
 
    using global::Model;
    using BLL;
    using System.Collections;
    using DbFrame.Class;
    using AdminManagement.AOP;
    using System.IO;

    public class MenuFunctionController : BaseController
    {

        Sys_MenuBL _Sys_MenuBL = new Sys_MenuBL();
        protected override void Init()
        {
           
            this.MenuID = "Z-130";
        }
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _Sys_MenuBL.GetDataSource(query, page, rows);
        }
        /// <summary>
        /// 获取菜单和功能树
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetMenuAndFunctionTree()
        {
            return Json(new { status = 1, value = _Sys_MenuBL.GetMenuAndFunctionTree() });
        }
        /// <summary>
        /// 功能模块
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public IActionResult Find(string ID) {

    


            return Json(_Sys_MenuBL.Find(ID.ToGuid()));
        }

        /// <summary>
        /// 功能模块
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [AopCheckEntityFilterAtttibute(new string[] { ("model") })]
        [HttpPost] 
        public IActionResult Save(Sys_MenuM model , string Function_ID)
        {
            this.KeyID = _Sys_MenuBL.Save(model, Function_ID);
            return Json(new { status = 1, ID = KeyID });
       
        }
        [HttpPost]
        public ActionResult Delete(string ID) {

            _Sys_MenuBL.Delete(ID);
            return Json(new { status = 1 });
        }





    }
}