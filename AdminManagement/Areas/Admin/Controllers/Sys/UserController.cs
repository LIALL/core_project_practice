﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Admin.Controllers.Sys
{
    using global::Model;
    using BLL;
    using DbFrame.Class;
    using System.Collections;
    using AOP;
    using System.IO;
    using System.Text;

    public class UserController : BaseController
    {
        Sys_UserBL _Sys_UserBL = new Sys_UserBL();

        protected override void Init()
        {
           // base.Init();
            this.MenuID = "Z-100";
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}
        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="query"></param>
        /// <param name="page">PageIndex</param>
        /// <param name="rows">PageSize</param>
        /// <returns></returns>
        [NonAction]   //只是普通的方法 
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _Sys_UserBL.GetDataSource(query, page, rows);
        }

        #region
        [HttpPost]
        public IActionResult Find(string ID) {

            return Json(_Sys_UserBL.Find(ID.ToGuid()));
        }
        [HttpPost] 
        public IActionResult Delete(string id) {

            _Sys_UserBL.Delete(id);
            return Json(new { status = 1 });
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Role_ID"></param>
        /// <returns></returns>

        [AopCheckEntityFilterAtttibute(new string[] { "model" })]
        [HttpPost]
        public IActionResult Save(Sys_UserM model, string Role_ID)
        {
            this.KeyID = _Sys_UserBL.Save(model, Role_ID);
            return Json(new { status = 1, ID = KeyID });
        }
        #endregion

    }
}