﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace AdminManagement.Areas.Admin.Controllers.Sys
{
    using global::Model;
    using Common;
    public class ChangePwdController :BaseController
    {
        Sys_UserM _Sys_UserM = new Sys_UserM();
        protected override void Init()
        {
           this.MenuID="Z-150";  
        }
        public override IActionResult Index()
        {

            _Sys_UserM = db.Find<Sys_UserM>(f => f.User_ID == Account.UserID);
            ViewData["UserName"] = _Sys_UserM.User_Name;
            ViewData["User_LoginName"] = _Sys_UserM.User_LoginName;
            return View();

        }
        public  IActionResult ChangePwd(string oldpwd,string newpwd,string newlypwd)
        {
            if (string.IsNullOrEmpty(oldpwd)) {
                throw new MessageBox("旧密码不能为空！");
            }
            if (string.IsNullOrEmpty(newpwd))
            {
                throw new MessageBox("新密码不能为空！");
            }
            if (string.IsNullOrEmpty(oldpwd))
            {
                throw new MessageBox("确认密码不能为空！");
            }
            if (string.IsNullOrEmpty(oldpwd))
            {
                throw new MessageBox("旧密码不能为空!");
            }
            _Sys_UserM = db.Find<Sys_UserM>(f => f.User_ID == Account.UserID);
            if (!_Sys_UserM.User_Pwd.Equals(oldpwd.Trim())) {

                throw new MessageBox("旧密码不正确！");
            }
            //修改
            if (!db.Edit<Sys_UserM>(() => new Sys_UserM() { User_Pwd = newlypwd }, p => p.User_ID == Account.UserID, li)) {
                throw new MessageBox(db.ErrorMessage);
            }
            
            if (!db.Commit(li)) {
                throw new MessageBox(db.ErrorMessage);
            }

            //JsonConvert.SerializeObject(new { y = 1 });

            //Sys_UserM ad= JsonConvert.DeserializeObject<Sys_UserM>(json);
            return  Json(new { status="Ok"}); 

        }

       

    }

}
