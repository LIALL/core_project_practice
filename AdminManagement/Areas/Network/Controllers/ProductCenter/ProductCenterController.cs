﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Network.Controllers.Product
{
    using System.Collections;
    using AdminManagement.Areas.Admin.Controllers;
    using BLL;
    using DbFrame.Class;
    using Model;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Hosting;
    [Area("Network")]   //增加区域
    public class ProductCenterController : BaseController
    {
        protected override void Init()
        {
            this.MenuID = "D-120";
        }

        public IHostingEnvironment _hostingEnvironment;
        public readonly string _Webrootpath;
        public ProductCenterController(IHostingEnvironment hostingEnvironment)
        {

            _hostingEnvironment = hostingEnvironment;
            _Webrootpath = _hostingEnvironment.WebRootPath;
        }
        Network_ProductBLL _ProductBLL = new Network_ProductBLL();
        Network_ProductM _network_ProductM = new Network_ProductM();
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _ProductBLL.GetDataSource(query, page, rows);
        }
        public ActionResult Find(string ID)
        {

            return Json(_ProductBLL.Find(ID.ToGuid()));
        }
        //public override IActionResult Info()
        //{
        //    Dictionary<string, string> dic = new Dictionary<string, string>();
        //    List<string> _list = new List<string>();
        //    _HomeBLL.Findtype(MenuID).ForEach(item =>
        //    {
        //        //  _list.Add(item.Menu_Name);
        //        // ViewData[item.Menu_Num] = item.Menu_Name;
        //        dic.Add(item.Menu_ID.ToStr(), item.Menu_Name);
        //    });
        //    ViewData["dic"] = dic;
        //    return View(); ;
        //}
        [HttpPost]
        public ActionResult Delete(string ID) {

            _ProductBLL.Delete(ID);
            return Json(new { status = "1" });
        }
        [AOP.AopCheckEntityFilterAtttibute(new string[] { "model" } )]
        [HttpPost]
        public ActionResult Save(Network_ProductM model, IFormFile Product_Image)
        {

            if (Product_Image != null)
            {
                this.HandleUpFile(Product_Image, new string[] { ".jpg", ".png", ".gif" }, _Webrootpath, null, (_path) =>
                {
                    model.Product_Image = _path;
                });
            }
            //if (Home_RotateImageURLOne != null)
            //{
            //    this.HandleUpFile(Home_RotateImageURLOne, new string[] { ".jpg", ".png", ".gif" }, _Webrootpath, null, (_path) =>
            //    {

            //        model.Home_RotateImageURLOne = _path;
            //    });
            //}
           
            this.KeyID = _ProductBLL.Save(model);

            return Json(new { status = "1", Id = this.KeyID });
        }
    }
}