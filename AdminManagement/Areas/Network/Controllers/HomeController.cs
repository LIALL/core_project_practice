﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Network.Controllers
{
    using System.Collections;
    using AdminManagement.Areas.Admin.Controllers;
    using BLL;
    using Model;

    [Area("Network")]
    public class HomeController : BaseController
    {
       // Dictionary<string, string> dic;
        protected override void Init()
        {
            this.MenuID = "D-110";
            this.PrintTitle = "HomemsgPrint";
           

        }
        Network_HomeM _HomeM= new Network_HomeM();
        Network_HomeBLL _HomeBLL = new Network_HomeBLL();
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _HomeBLL.GetDataSource(query, page, rows);
        }
        [HttpPost]
        public ActionResult Find() {
           
            return null;
        }
        [HttpPost]
        public ActionResult Findtype(string MenuID)
        {
            // ViewData[]
          
            return  Json( _HomeBLL.Findtype(MenuID));
        }
        //

    }
}