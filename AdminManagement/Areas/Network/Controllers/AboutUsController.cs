﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Network.Controllers
{
    using System.Collections;
    using AdminManagement.Areas.Admin.Controllers;
    using BLL;
    using DbFrame.Class;
    using Model;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Hosting;
    [Area("Network")]
    public class AboutUsController : BaseController
    {

        protected override void Init()
        {
            this.MenuID = "D-100-0";


        }
        public IHostingEnvironment _hostingEnvironment;
        public readonly string _Webrootpath;
        public AboutUsController(IHostingEnvironment hostingEnvironment) {

            _hostingEnvironment = hostingEnvironment;
            _Webrootpath = _hostingEnvironment.WebRootPath;
        }
        Network_HomeM _HomeM = new Network_HomeM();
        Network_HomeBLL _HomeBLL = new Network_HomeBLL();
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            return _HomeBLL.GetDataSource(query, page, rows);
        }
        [HttpPost]
        public ActionResult Find(string ID)
        {
           
            return Json(_HomeBLL.Find(ID.ToGuid()));
        }
        public override IActionResult Info()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            List<string> _list = new List<string>();
            _HomeBLL.Findtype(MenuID).ForEach(item =>
            {
              //  _list.Add(item.Menu_Name);
                // ViewData[item.Menu_Num] = item.Menu_Name;
                dic.Add(item.Menu_ID.ToStr(), item.Menu_Name);
            });
            ViewData["dic"] = dic;
            return View(); ;
        }
        [HttpPost]
        public ActionResult Findtype(string MenuID)
        {
            // ViewData[]

            return Json(_HomeBLL.Findtype(MenuID));
        }
        [HttpPost]
        public ActionResult Save(Network_HomeM model,IFormFile Home_RotateImageURL, IFormFile Home_RotateImageURLOne, IFormFile Home_RotateImageURLTwo,IFormFile Home_RotateImageURLThree) {

            if (Home_RotateImageURL != null) {
                this.HandleUpFile(Home_RotateImageURL, new string[] { ".jpg", ".png", ".gif" }, _Webrootpath, null, (_path) =>
                {

                    model.Home_RotateImageURL = _path;
                });
            }
            if (Home_RotateImageURLOne != null)
            {
                this.HandleUpFile(Home_RotateImageURLOne, new string[] { ".jpg", ".png", ".gif" }, _Webrootpath, null, (_path) =>
                {

                    model.Home_RotateImageURLOne = _path;
                });
            }
            if (Home_RotateImageURLTwo != null)
            {
                this.HandleUpFile(Home_RotateImageURLTwo, new string[] { ".jpg", ".png", ".gif" }, _Webrootpath, null, (_path) =>
                {

                    model.Home_RotateImageURLTwo = _path;
                });

            }
            if (Home_RotateImageURLThree != null)
            {
                this.HandleUpFile(Home_RotateImageURLThree, new string[] { ".jpg", ".png", ".gif" }, _Webrootpath, null, (_path) =>
                {

                    model.Home_RotateImageURLThree = _path;
                });

            }
        
          

            this.KeyID = _HomeBLL.Save(model);

            return Json(new { status = "1", Id = this.KeyID });
        }
    }
}