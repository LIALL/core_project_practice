﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.Areas.Network.Controllers
{
    using System.Collections;
    using AdminManagement.Areas.Admin.Controllers;
    using BLL;
    using DbFrame.Class;
    using Model;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Hosting;
    [Area("Network")]   //增加区域
    public class NewSController : BaseController

    {
        protected override void Init()
        {
            this.MenuID = "D-130";
        }
    }
}