using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
	//
	using Models;
	using DAL;
	using System.Collections;
	using BLL.Class;
	using Common;
	using DbFrame;
	using DbFrame.Class;

    public class Sys_NumberBLL : BaseBLL
    {
        Sys_NumberM _Sys_NumberM = new Sys_NumberM();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
            return new Sys_NumberDA().GetDataSource(query, page, rows);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(Sys_NumberM model)
        {
			/*
			if (model.Role_ID.ToGuid().Equals(Guid.Empty))
            {
                model.Role_ID = db.Add(model, li).ToGuid();
                if (model.Role_ID.Equals(Guid.Empty))
                    throw new MessageBox(db.ErrorMessge);
            }
            else
            {
                if (!db.EditById(model,li))
                    throw new MessageBox(db.ErrorMessge);
            }

            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);

            return model.Role_ID.ToGuidStr();
			*/
			return null;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
		    db.JsonToList<string>(ID).ForEach(item =>
            {
                if (!db.DeleteById<Sys_NumberM>(item.ToGuid(),  li))
                    throw new MessageBox(db.ErrorMessge);
            });
            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);
            return true;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {
          /*
			var _Sys_NumberM = db.FindById<Sys_NumberM>(ID);
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"_Sys_NumberM",_Sys_NumberM},
                {"status",1}
            });
            return di;
			*/
			return null;
        }

    }
}
