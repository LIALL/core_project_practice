using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
	//
	using Models;
	using DAL;
	using System.Collections;
	using BLL.Class;
	using Common;
	using DbFrame;
	using DbFrame.Class;

    public class Network_ContectUsBLL : BaseBLL
    {
        Network_ContectUsM _Network_ContectUsM = new Network_ContectUsM();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
            return new Network_ContectUsDA().GetDataSource(query, page, rows);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(Network_ContectUsM model)
        {
			/*
			if (model.Role_ID.ToGuid().Equals(Guid.Empty))
            {
                model.Role_ID = db.Add(model, li).ToGuid();
                if (model.Role_ID.Equals(Guid.Empty))
                    throw new MessageBox(db.ErrorMessge);
            }
            else
            {
                if (!db.EditById(model,li))
                    throw new MessageBox(db.ErrorMessge);
            }

            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);

            return model.Role_ID.ToGuidStr();
			*/
			return null;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
		    db.JsonToList<string>(ID).ForEach(item =>
            {
                if (!db.DeleteById<Network_ContectUsM>(item.ToGuid(),  li))
                    throw new MessageBox(db.ErrorMessge);
            });
            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);
            return true;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {
          /*
			var _Network_ContectUsM = db.FindById<Network_ContectUsM>(ID);
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"_Network_ContectUsM",_Network_ContectUsM},
                {"status",1}
            });
            return di;
			*/
			return null;
        }

    }
}
