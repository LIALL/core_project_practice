using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Network_Home")]
    public class Network_HomeM : BaseEntity<Network_HomeM>
    {
		[Field("Home_ID", IsPrimaryKey = true)]
		public Guid Home_ID { get; set; }

		[Field("Home_RotateImageURL")]
		public string Home_RotateImageURL { get; set; }

		[Field("Home_Contacttype")]
		public string Home_Contacttype { get; set; }

		[Field("Home_ContentFonter")]
		public string Home_ContentFonter { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Home_CreateTime { get; set; }


    }
}
