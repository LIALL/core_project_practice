using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_User")]
    public class Sys_UserM : BaseEntity<Sys_UserM>
    {
		[Field("User_ID", IsPrimaryKey = true)]
		public Guid User_ID { get; set; }

		[Field("User_Name")]
		public string User_Name { get; set; }

		[Field("User_LoginName")]
		public string User_LoginName { get; set; }

		[Field("User_Pwd")]
		public string User_Pwd { get; set; }

		[Field("User_Email")]
		public string User_Email { get; set; }

		[Field("User_IsDelete")]
		public int? User_IsDelete { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? User_CreateTime { get; set; }


    }
}
