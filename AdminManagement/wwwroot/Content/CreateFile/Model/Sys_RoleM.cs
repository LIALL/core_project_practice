using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_Role")]
    public class Sys_RoleM : BaseEntity<Sys_RoleM>
    {
		[Field("Role_ID", IsPrimaryKey = true)]
		public Guid Role_ID { get; set; }

		[Field("Role_Num")]
		public string Role_Num { get; set; }

		[Field("Role_Name")]
		public string Role_Name { get; set; }

		[Field("Role_Remark")]
		public string Role_Remark { get; set; }

		[Field("Role_IsDelete")]
		public int? Role_IsDelete { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Role_CreateTime { get; set; }


    }
}
