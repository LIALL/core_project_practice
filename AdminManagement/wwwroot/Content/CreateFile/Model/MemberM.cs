using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Member")]
    public class MemberM : BaseEntity<MemberM>
    {
		[Field("Member_ID", IsPrimaryKey = true)]
		public Guid Member_ID { get; set; }

		[Field("Member_Num")]
		public string Member_Num { get; set; }

		[Field("Member_Name")]
		public string Member_Name { get; set; }

		[Field("Member_Phone")]
		public int? Member_Phone { get; set; }

		[Field("Member_Sex")]
		public string Member_Sex { get; set; }

		[Field("Member_Birthday")]
		public DateTime? Member_Birthday { get; set; }

		[Field("Member_Photo")]
		public string Member_Photo { get; set; }

		[Field("Member_UserID")]
		public Guid? Member_UserID { get; set; }

		[Field("Member_Introduce")]
		public string Member_Introduce { get; set; }

		[Field("Member_FilePath")]
		public string Member_FilePath { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Member_CreateTime { get; set; }


    }
}
