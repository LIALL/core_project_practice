using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("SaleProduct")]
    public class SaleProductM : BaseEntity<SaleProductM>
    {
		[Field("SaleProduct_ID", IsPrimaryKey = true)]
		public Guid SaleProduct_ID { get; set; }

		[Field("SaleProduct_OwerComanyName")]
		public string SaleProduct_OwerComanyName { get; set; }

		[Field("SaleProduct_Province")]
		public string SaleProduct_Province { get; set; }

		[Field("SaleProduct_City")]
		public string SaleProduct_City { get; set; }

		[Field("SaleProduct_LinkMan")]
		public string SaleProduct_LinkMan { get; set; }

		[Field("SaleProduct_SfyNum")]
		public string SaleProduct_SfyNum { get; set; }

		[Field("SaleProduct_SfyPzRemark")]
		public string SaleProduct_SfyPzRemark { get; set; }

		[Field("SaleProduct_SxyNum")]
		public string SaleProduct_SxyNum { get; set; }

		[Field("SaleProduct_SaleDatetime")]
		public DateTime? SaleProduct_SaleDatetime { get; set; }

		[Field("SaleProduct_PrincipalMan")]
		public string SaleProduct_PrincipalMan { get; set; }

		[Field("SaleProduct_AllRemark")]
		public string SaleProduct_AllRemark { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? SaleProduct__CreateTime { get; set; }


    }
}
