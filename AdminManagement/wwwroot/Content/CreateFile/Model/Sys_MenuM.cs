using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_Menu")]
    public class Sys_MenuM : BaseEntity<Sys_MenuM>
    {
		[Field("Menu_ID", IsPrimaryKey = true)]
		public Guid Menu_ID { get; set; }

		[Field("Menu_Num")]
		public string Menu_Num { get; set; }

		[Field("Menu_Name")]
		public string Menu_Name { get; set; }

		[Field("Menu_Url")]
		public string Menu_Url { get; set; }

		[Field("Menu_Icon")]
		public string Menu_Icon { get; set; }

		[Field("Menu_ParentID")]
		public Guid? Menu_ParentID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Menu_CreateTime { get; set; }


    }
}
