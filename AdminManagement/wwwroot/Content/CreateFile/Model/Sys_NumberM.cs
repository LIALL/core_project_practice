using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_Number")]
    public class Sys_NumberM : BaseEntity<Sys_NumberM>
    {
		[Field("Number_ID", IsPrimaryKey = true)]
		public Guid Number_ID { get; set; }

		[Field("Number_Num")]
		public string Number_Num { get; set; }

		[Field("Number_DataBase")]
		public string Number_DataBase { get; set; }

		[Field("Number_TableName")]
		public string Number_TableName { get; set; }

		[Field("Number_NumField")]
		public string Number_NumField { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Number_CreateTime { get; set; }


    }
}
