using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Network_NewsCenter")]
    public class Network_NewsCenterM : BaseEntity<Network_NewsCenterM>
    {
		[Field("NewsCenter_ID", IsPrimaryKey = true)]
		public Guid NewsCenter_ID { get; set; }

		[Field("NewsCenter_title")]
		public string NewsCenter_title { get; set; }

		[Field("NewsCenter_Abstract")]
		public string NewsCenter_Abstract { get; set; }

		[Field("NewsCenter_Content")]
		public string NewsCenter_Content { get; set; }

		[Field("NewsCenter_IsTop")]
		public string NewsCenter_IsTop { get; set; }

		[Field("NewsCenter_lrr")]
		public string NewsCenter_lrr { get; set; }

		[Field("NewsCenter_MenuID")]
		public string NewsCenter_MenuID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? NewsCenter_CreateTime { get; set; }


    }
}
