using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Network_ContectUs")]
    public class Network_ContectUsM : BaseEntity<Network_ContectUsM>
    {
		[Field("ContectUs_ID", IsPrimaryKey = true)]
		public Guid ContectUs_ID { get; set; }

		[Field("ContectUs_Content")]
		public string ContectUs_Content { get; set; }

		[Field("ContectUs_Lrr")]
		public string ContectUs_Lrr { get; set; }

		[Field("Recruitment_Createtime")]
		public DateTime? Recruitment_Createtime { get; set; }


    }
}
