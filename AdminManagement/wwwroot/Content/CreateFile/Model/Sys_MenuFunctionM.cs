using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_MenuFunction")]
    public class Sys_MenuFunctionM : BaseEntity<Sys_MenuFunctionM>
    {
		[Field("MenuFunction_ID", IsPrimaryKey = true)]
		public Guid MenuFunction_ID { get; set; }

		[Field("MenuFunction_MenuID")]
		public Guid? MenuFunction_MenuID { get; set; }

		[Field("MenuFunction_FunctionID")]
		public Guid? MenuFunction_FunctionID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? MenuFunction_CreateTime { get; set; }


    }
}
