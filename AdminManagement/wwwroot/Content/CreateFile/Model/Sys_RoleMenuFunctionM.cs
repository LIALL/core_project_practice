using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_RoleMenuFunction")]
    public class Sys_RoleMenuFunctionM : BaseEntity<Sys_RoleMenuFunctionM>
    {
		[Field("RoleMenuFunction_ID", IsPrimaryKey = true)]
		public Guid RoleMenuFunction_ID { get; set; }

		[Field("RoleMenuFunction_RoleID")]
		public Guid? RoleMenuFunction_RoleID { get; set; }

		[Field("RoleMenuFunction_FunctionID")]
		public Guid? RoleMenuFunction_FunctionID { get; set; }

		[Field("RoleMenuFunction_MenuID")]
		public Guid? RoleMenuFunction_MenuID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? RoleMenuFunction_CreateTime { get; set; }


    }
}
