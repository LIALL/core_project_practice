using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_UserRole")]
    public class Sys_UserRoleM : BaseEntity<Sys_UserRoleM>
    {
		[Field("UserRole_ID")]
		public Guid? UserRole_ID { get; set; }

		[Field("UserRole_ID", IsPrimaryKey = true)]
		public Guid UserRole_ID { get; set; }

		[Field("UserRole_UserID", IsPrimaryKey = true)]
		public Guid UserRole_UserID { get; set; }

		[Field("UserRole_UserID")]
		public Guid? UserRole_UserID { get; set; }

		[Field("UserRole_RoleID")]
		public Guid? UserRole_RoleID { get; set; }

		[Field("UserRole_RoleID")]
		public Guid? UserRole_RoleID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? UserRole_CreateTime { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? UserRole_CreateTime { get; set; }


    }
}
