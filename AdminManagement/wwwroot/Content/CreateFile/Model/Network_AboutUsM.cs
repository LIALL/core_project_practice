using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Network_AboutUs")]
    public class Network_AboutUsM : BaseEntity<Network_AboutUsM>
    {
		[Field("Conpany_ID", IsPrimaryKey = true)]
		public Guid Conpany_ID { get; set; }

		[Field("Conpany_Introduce")]
		public string Conpany_Introduce { get; set; }

		[Field("NewConpany_Image")]
		public string NewConpany_Image { get; set; }

		[Field("Conpany_MenuID")]
		public string Conpany_MenuID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Conpany_CreateTime { get; set; }


    }
}
