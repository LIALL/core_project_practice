using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Network_Product")]
    public class Network_ProductM : BaseEntity<Network_ProductM>
    {
		[Field("Product_ID", IsPrimaryKey = true)]
		public Guid Product_ID { get; set; }

		[Field("Product_Introduce")]
		public string Product_Introduce { get; set; }

		[Field("Product_Feature")]
		public string Product_Feature { get; set; }

		[Field("Product_Funtion")]
		public string Product_Funtion { get; set; }

		[Field("Product_News")]
		public string Product_News { get; set; }

		[Field("Product_Image")]
		public string Product_Image { get; set; }

		[Field("Product_MenuID")]
		public string Product_MenuID { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Product_CreateTime { get; set; }


    }
}
