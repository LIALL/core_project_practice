using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("XsDownLoad")]
    public class XsDownLoadM : BaseEntity<XsDownLoadM>
    {
		[Field("XsDownLoad_ID", IsPrimaryKey = true)]
		public Guid XsDownLoad_ID { get; set; }

		[Field("XsDownLoad_MachineName")]
		public string XsDownLoad_MachineName { get; set; }

		[Field("XsDownLoad_MachineUrl")]
		public string XsDownLoad_MachineUrl { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? XsDownLoad__CreateTime { get; set; }


    }
}
