using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model{
	//
	using DbFrame.Class;

	[Table("Sys_Function")]
    public class Sys_FunctionM : BaseEntity<Sys_FunctionM>
    {
		[Field("Function_ID", IsPrimaryKey = true)]
		public Guid Function_ID { get; set; }

		[Field("Function_Num")]
		public string Function_Num { get; set; }

		[Field("Function_Name")]
		public string Function_Name { get; set; }

		[Field("Function_ByName")]
		public string Function_ByName { get; set; }

		[Field("创建时间", IsIgnore = true)]
		public DateTime? Function_CreateTime { get; set; }


    }
}
