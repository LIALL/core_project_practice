﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminManagement.AOP
{
    using Microsoft.AspNetCore.Mvc.Filters;
    using Common;
    using DbFrame.Class;
    using DbFrame;
    public class AopCheckEntityFilterAtttibute :ActionFilterAttribute
    {
        private DBContext db = new DBContext();

        private string[] ParamName { get; set; }

        public AopCheckEntityFilterAtttibute(string[] _ParamName) {

            ParamName = _ParamName;
        }
        /// <summary>
        /// 验证实体  
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (ParamName != null) {


                foreach (var item in ParamName) {

                    var _Value = (EntityClass)context.ActionArguments[item];
                    if (_Value != null) {

                        if (!db.CheckModel(_Value)) {
                           
                            throw new MessageBox(db.ErrorMessage);
                        }
                    }
                }
            }
        }
    }
}
