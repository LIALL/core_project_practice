﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
namespace AdminManagement.AOP
{
    using Microsoft.AspNetCore.Mvc.Filters;
    using Common;
    using Model;

    using DbFrame.Class;
    //过滤器拦截
    public class AopActionFilterAttribute :ActionFilterAttribute
    {
        private bool _IsExecute { get; set; }

        public AopActionFilterAttribute(bool IsExecute=true) {

            this._IsExecute = IsExecute;

        }
        /// <summary>
        /// 超时验证过滤器 行为执行前
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (this._IsExecute) {
                //登录超时验证
                CheckLoginAccount(context);
            }
            base.OnActionExecuting(context);
        }
        /// <summary>
        /// 检查登录帐户
        /// </summary>
        public void CheckLoginAccount(ActionExecutingContext context) {


            var accountM = Tools.GetSession<Sys_AccountM>("Account");


            if (accountM == null || accountM.UserID.ToGuid() == Guid.Empty) {

                if (Tools.IsAjaxRequest) {

                    context.Result = new JsonResult(new ErrorModel(AppConfig.LoginPageUrl, EMsgStatus.登录超时20));

                }
                else
                {
                    context.Result = new ContentResult() {
                        Content = @"<script type='type/javascript'> alert('登录超时，返回至登录页面');
                                 top.window.location='" + AppConfig.LoginPageUrl + "' </ script > ",
                        ContentType = "txet/html;charset=utf-8;"   
                    };
                }
            }
            else {
                var _Controller = context.Controller as AdminManagement.Areas.Admin.Controllers.BaseController;
                DAL.Sys_AppLogDA.InsertAppLog(context.HttpContext, _Controller.Account.UserID);
            }
            
            //Areas.Admin.Controllers.Sys.AppLogController _appLogController = new Areas.Admin.Controllers.Sys.AppLogController();
            //_appLogController.InsertAppLog(context.HttpContext, _Controller.Account.UserID);

        }

    }
}