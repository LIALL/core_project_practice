#pragma checksum "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "03ec5f96fe4d5c69e6e2b3afc6eadd1501d941cc"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Network_Views_HomeSet_Info), @"mvc.1.0.view", @"/Areas/Network/Views/HomeSet/Info.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Network/Views/HomeSet/Info.cshtml", typeof(AspNetCore.Areas_Network_Views_HomeSet_Info))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 5 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\_ViewImports.cshtml"
using DbFrame;

#line default
#line hidden
#line 6 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\_ViewImports.cshtml"
using DbFrame.Class;

#line default
#line hidden
#line 7 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\_ViewImports.cshtml"
using Model;

#line default
#line hidden
#line 8 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\_ViewImports.cshtml"
using Common;

#line default
#line hidden
#line 9 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\_ViewImports.cshtml"
using AppHtml;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"03ec5f96fe4d5c69e6e2b3afc6eadd1501d941cc", @"/Areas/Network/Views/HomeSet/Info.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"384cda24c74e1bc459064a15967bffb875076692", @"/Areas/Network/_ViewImports.cshtml")]
    public class Areas_Network_Views_HomeSet_Info : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/Ko/Knockout-3.4.2.debug.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/js/admin-form.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/layDate-v5.0.7/laydate/laydate.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/nUeditor/neditor.config.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/nUeditor/neditor.all.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/nUeditor/i18n/zh-cn/zh-cn.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
  
    var KeyID = Tools.HttpHelper.Request.Query["ID"];
    var Sexlist = new List<string>();
    Sexlist.Add("男");
    Sexlist.Add("女");


#line default
#line hidden
            BeginContext(151, 194, true);
            WriteLiteral("<div class=\"page-content animated fadeInDown \">\r\n\r\n    <div class=\"container-fluid\" style=\"padding-bottom:100px;\">\r\n\r\n        <div class=\"row\" id=\"form\">\r\n\r\n            <!--检索面板-->\r\n            ");
            EndContext();
            BeginContext(347, 796, false);
#line 16 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(UI.Select<Network_HomeM>(item => item.Home_Contacttype, () =>
                                                               {
                                                                   var Html = "";
                                                                   foreach (var item in ViewData["dic"] as Dictionary<string, string>)
                                                                   {
                                                                       Html += "<option value=\"" + item.Key + "\">" + item.Value +"不要管我！！" + "</option>";
                                                                   }
                                                                   return Html;


                                                               },6));

#line default
#line hidden
            EndContext();
            BeginContext(1144, 16, true);
            WriteLiteral("\r\n\r\n            ");
            EndContext();
            BeginContext(1162, 58, false);
#line 28 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(UI.Input<Network_HomeM>(item => item.Home_Conpany_Title,6));

#line default
#line hidden
            EndContext();
            BeginContext(1221, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1237, 66, false);
#line 29 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(UI.UploadImage<Network_HomeM>(item => item.Home_RotateImageURL, 6));

#line default
#line hidden
            EndContext();
            BeginContext(1304, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1320, 69, false);
#line 30 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(UI.UploadImage<Network_HomeM>(item => item.Home_RotateImageURLOne, 6));

#line default
#line hidden
            EndContext();
            BeginContext(1390, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1406, 69, false);
#line 31 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(UI.UploadImage<Network_HomeM>(item => item.Home_RotateImageURLTwo, 6));

#line default
#line hidden
            EndContext();
            BeginContext(1476, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1492, 71, false);
#line 32 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(UI.UploadImage<Network_HomeM>(item => item.Home_RotateImageURLThree, 6));

#line default
#line hidden
            EndContext();
            BeginContext(1564, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1580, 62, false);
#line 33 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(UI.UEditor<Network_HomeM>(item => item.Home_ContentFonter, 12));

#line default
#line hidden
            EndContext();
            BeginContext(1643, 495, true);
            WriteLiteral(@"
        </div>
    </div>

</div>
<!--按钮-->
<div class=""my-ButtonBar text-right"">
    <div class=""btn-group"">
        <button type=""button"" class=""btn btn-success btn-outline"" data-bind=""visible: !App.IsAdd()"" onclick=""adminForm.resetUrl(null)"">新增</button>
        <button class=""btn btn-primary btn-outline"" onclick=""App.Save()"">提交</button>
        <button class=""btn btn-danger btn-outline"" onclick=""admin.layer.close(admin.getLayerIframeIndex());"">关闭</button>
    </div>
</div>
");
            EndContext();
            DefineSection("css", async() => {
                BeginContext(2151, 6, true);
                WriteLiteral("\r\n\r\n\r\n");
                EndContext();
            }
            );
            DefineSection("js", async() => {
                BeginContext(2172, 8, true);
                WriteLiteral("\r\n\r\n    ");
                EndContext();
                BeginContext(2180, 62, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "72f6eebbd7e5475d95e0f56eec158e98", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2242, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(2248, 48, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "de7d37c3384d4586982ba9b59964b7ef", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2296, 23, true);
                WriteLiteral("\r\n    <!--时间插件-->\r\n    ");
                EndContext();
                BeginContext(2319, 69, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1a6adf4866d842e394b3be3a5080ba5c", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2388, 24, true);
                WriteLiteral("\r\n    <!--编辑器文件-->\r\n    ");
                EndContext();
                BeginContext(2412, 62, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0653d59ef4504a9ab8f38fbec4fb0efe", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2474, 26, true);
                WriteLiteral("\r\n    <!--编辑器源码文件-->\r\n    ");
                EndContext();
                BeginContext(2500, 63, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "669d348ef22c46818c76f5b226dad6b4", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2563, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(2569, 64, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "eaf6e86f04b2407999803f4fef6fbd03", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2633, 79, true);
                WriteLiteral("\r\n    <!--业务代码-->\r\n    <script type=\"text/javascript\">\r\n\r\n        var KeyId = \'");
                EndContext();
                BeginContext(2713, 5, false);
#line 64 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
                Write(KeyID);

#line default
#line hidden
                EndContext();
                BeginContext(2718, 977, true);
                WriteLiteral(@"';
        model = new vModel();
        $(function () {
            ko.applyBindings(model);
            App.Load();
        });
        var App = {
            IsAdd: function () {

                return !KeyId;
            },
            Load: function () {
                //时间配置代码
                //laydate.render({
                //    elem: ""input[name=Member_Birthday]"",
                //    type: 'date',
                //    done: function (value, date, endDate) {
                //        model.Member_Birthday(value);
                //    }
                //});
                //editor编辑器 配置
                var editor = UE.getEditor(""Home_ContentFonter"");
                editor.addListener('blur', function (editor) {

                    model.Home_ContentFonter(UE.getEditor('Home_ContentFonter').getContent());
                });
                adminForm.load({
                    KeyId: KeyId,
                    url: """);
                EndContext();
                BeginContext(3696, 18, false);
#line 92 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
                     Write(Url.Action("Find"));

#line default
#line hidden
                EndContext();
                BeginContext(3714, 380, true);
                WriteLiteral(@""",
                    callBack: function (r) {
                        setTimeout(function () {
                            editor.setContent(r.Home_ContentFonter ? r.Home_ContentFonter.replace(/'/g, '\'') : """");
                        }, 300);
                    }
                });


            },
            Save: function () {
                var loadUrl = """);
                EndContext();
                BeginContext(4095, 18, false);
#line 103 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
                          Write(Url.Action("Save"));

#line default
#line hidden
                EndContext();
                BeginContext(4113, 1114, true);
                WriteLiteral(@""";
                var newData = adminForm.createFormData({
                    Home_RotateImageURL: $(""Input[name=Home_RotateImageURL]"")[0].files[0],
                    Home_RotateImageURLOne: $(""Input[name=Home_RotateImageURLOne]"")[0].files[0],
                    Home_RotateImageURLTwo: $(""Input[name=Home_RotateImageURLTwo]"")[0].files[0],
                    Home_RotateImageURLThree: $(""Input[name=Home_RotateImageURLThree]"")[0].files[0]
                });

                adminForm.Save({
                    url: loadUrl,
                    data: newData,
                    isupfile: true
                });
            },
            FindBack: {//查找带回类
                CallBack: function (row, tag, dom) {
                    if (row != null && row.length > 0) row = row[0];
                    if (tag == ""User"") {
                        model.Member_UserID(row ? row._ukid : """");
                        model.User_Name(row ? row.User_Name : """");
                    }
               ");
                WriteLiteral(" }\r\n            },\r\n            Delete: function () {\r\n                 adminList.delete(\'");
                EndContext();
                BeginContext(5228, 20, false);
#line 127 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
                              Write(Url.Action("Delete"));

#line default
#line hidden
                EndContext();
                BeginContext(5248, 240, true);
                WriteLiteral("\', function () {\r\n                     App.Refresh();\r\n                     console.log(\"删除完成！\");\r\n                 });\r\n             },\r\n             //导出\r\n             ExportExecl: function () {\r\n\r\n                 adminList.exportExcel(\"");
                EndContext();
                BeginContext(5489, 25, false);
#line 135 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
                                   Write(Url.Action("ExportExecl"));

#line default
#line hidden
                EndContext();
                BeginContext(5514, 215, true);
                WriteLiteral("\");\r\n             },\r\n             //刷新\r\n             Refresh: function (data) {\r\n\r\n                 adminList.refresh(data);\r\n             },\r\n             Print: function () {\r\n\r\n                 adminList.print(\'");
                EndContext();
                BeginContext(5730, 19, false);
#line 144 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
                             Write(Url.Action("Print"));

#line default
#line hidden
                EndContext();
                BeginContext(5749, 80, true);
                WriteLiteral("\');\r\n            }\r\n\r\n\r\n        };\r\n\r\n         function vModel() {\r\n            ");
                EndContext();
                BeginContext(5831, 72, false);
#line 151 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Network\Views\HomeSet\Info.cshtml"
        Write(
               UI.CreateKOViewModel(new Network_HomeM())
            );

#line default
#line hidden
                EndContext();
                BeginContext(5904, 30, true);
                WriteLiteral("\r\n        }\r\n\r\n    </script>\r\n");
                EndContext();
            }
            );
            BeginContext(5937, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
