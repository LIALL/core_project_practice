#pragma checksum "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a00b9994b83061b92cb4bac54731ff021cec9ef2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Base_Member_Info), @"mvc.1.0.view", @"/Areas/Admin/Views/Base/Member/Info.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Admin/Views/Base/Member/Info.cshtml", typeof(AspNetCore.Areas_Admin_Views_Base_Member_Info))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 5 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\_ViewImports.cshtml"
using DbFrame;

#line default
#line hidden
#line 6 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\_ViewImports.cshtml"
using DbFrame.Class;

#line default
#line hidden
#line 7 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\_ViewImports.cshtml"
using Model;

#line default
#line hidden
#line 8 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\_ViewImports.cshtml"
using Common;

#line default
#line hidden
#line 9 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\_ViewImports.cshtml"
using AppHtml;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a00b9994b83061b92cb4bac54731ff021cec9ef2", @"/Areas/Admin/Views/Base/Member/Info.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"384cda24c74e1bc459064a15967bffb875076692", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Base_Member_Info : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/Ko/Knockout-3.4.2.debug.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/js/admin-form.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/layDate-v5.0.7/laydate/laydate.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/nUeditor/neditor.config.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/nUeditor/neditor.all.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Admin/lib/nUeditor/i18n/zh-cn/zh-cn.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
  
    var KeyID = Tools.HttpHelper.Request.Query["ID"];
    var Sexlist = new List<string>();
    Sexlist.Add("男");
    Sexlist.Add("女");

#line default
#line hidden
            BeginContext(149, 192, true);
            WriteLiteral("<div class=\"page-content animated fadeInDown \">\r\n\r\n    <div class=\"container-fluid\" style=\"padding-bottom:100px;\">\r\n\r\n        <div class=\"row\" id=\"form\">\r\n            <!--检索面板-->\r\n            ");
            EndContext();
            BeginContext(343, 76, false);
#line 14 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.Input<MemberM>(item => item.Member_Num, 6, null, new { type = "number" }));

#line default
#line hidden
            EndContext();
            BeginContext(420, 16, true);
            WriteLiteral("\r\n\r\n            ");
            EndContext();
            BeginContext(438, 43, false);
#line 16 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.Input<MemberM>(item => item.Member_Name));

#line default
#line hidden
            EndContext();
            BeginContext(482, 16, true);
            WriteLiteral("\r\n\r\n            ");
            EndContext();
            BeginContext(500, 77, false);
#line 18 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.Input<MemberM>(item => item.Member_Phone, 6, null, new { type = "nuber" }));

#line default
#line hidden
            EndContext();
            BeginContext(578, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(594, 728, false);
#line 19 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.Select<MemberM>(item => item.Member_Sex, () =>
                                                                {
                                                                    var Html = "";
                                                                    foreach (var item in Sexlist)
                                                                    {
                                                                        Html += "<option value=\"" + item + "\">" + item + "</option>";
                                                                    }
                                                                    return Html;
                                                                }));

#line default
#line hidden
            EndContext();
            BeginContext(1323, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1339, 47, false);
#line 28 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.Input<MemberM>(item => item.Member_Birthday));

#line default
#line hidden
            EndContext();
            BeginContext(1387, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1403, 441, false);
#line 29 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.FindBack<Sys_UserM, MemberM>(U => U.User_Name, U => U.Member_UserID,
                                                       Url.Action("Index", "User", new { findback = "Multiple" }),
                                                       "App.FindBack.CallBack(row,'User')",
                                                                "App.FindBack.CallBack(null,'User');"

                                                     ));

#line default
#line hidden
            EndContext();
            BeginContext(1845, 16, true);
            WriteLiteral("\r\n\r\n            ");
            EndContext();
            BeginContext(1863, 50, false);
#line 36 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.UploadImage<MemberM>(item => item.Member_Photo));

#line default
#line hidden
            EndContext();
            BeginContext(1914, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1930, 52, false);
#line 37 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.UploadFile<MemberM>(item => item.Member_FilePath));

#line default
#line hidden
            EndContext();
            BeginContext(1983, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(1999, 54, false);
#line 38 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(UI.UEditor<MemberM>(item => item.Member_Introduce, 12));

#line default
#line hidden
            EndContext();
            BeginContext(2054, 502, true);
            WriteLiteral(@"



        </div>
     </div>

</div>
<!--按钮-->
<div class=""my-ButtonBar text-right"">
    <div class=""btn-group"">
        <button type=""button"" class=""btn btn-success btn-outline"" data-bind=""visible: !App.IsAdd()"" onclick=""adminForm.resetUrl(null)"">新增</button>
        <button class=""btn btn-primary btn-outline"" onclick=""App.Save()"">提交</button>
        <button class=""btn btn-danger btn-outline"" onclick=""admin.layer.close(admin.getLayerIframeIndex());"">关闭</button>
    </div>
</div>
");
            EndContext();
            DefineSection("css", async() => {
                BeginContext(2569, 6, true);
                WriteLiteral("\r\n\r\n\r\n");
                EndContext();
            }
            );
            DefineSection("js", async() => {
                BeginContext(2590, 8, true);
                WriteLiteral("\r\n\r\n    ");
                EndContext();
                BeginContext(2598, 62, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "87e43b115f7141c2a91e0e21c7d8b358", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2660, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(2666, 48, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ae2abaa87ca047c595e70e60f3eacb63", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2714, 23, true);
                WriteLiteral("\r\n    <!--时间插件-->\r\n    ");
                EndContext();
                BeginContext(2737, 69, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e214cf8b301e471cb958f2275987695b", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2806, 24, true);
                WriteLiteral("\r\n    <!--编辑器文件-->\r\n    ");
                EndContext();
                BeginContext(2830, 62, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "53227d860450468da067d36caed0d3b8", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2892, 26, true);
                WriteLiteral("\r\n    <!--编辑器源码文件-->\r\n    ");
                EndContext();
                BeginContext(2918, 63, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1dae9b0240564d4ea1c1e0d43d654f48", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2981, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(2987, 64, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ad833c5da8264625ad67e0fc0c2d1923", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3051, 86, true);
                WriteLiteral("\r\n    <!--业务代码-->\r\n    <script type=\"text/javascript\">\r\n       \r\n        var KeyId = \'");
                EndContext();
                BeginContext(3138, 5, false);
#line 72 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
                Write(KeyID);

#line default
#line hidden
                EndContext();
                BeginContext(3143, 957, true);
                WriteLiteral(@"';
        model = new vModel();
        $(function () {
            ko.applyBindings(model);
            App.Load();
        });
        var App = {
            IsAdd: function () {

                return !KeyId;
            },
            Load: function () {
                //时间配置代码
                laydate.render({
                    elem: ""input[name=Member_Birthday]"",
                    type: 'date',
                    done: function (value, date, endDate) {
                        model.Member_Birthday(value);
                    }
                });
                //editor编辑器 配置
                var editor = UE.getEditor(""Member_Introduce"");
                editor.addListener('blur', function (editor) {

                    model.Member_Introduce(UE.getEditor('Member_Introduce').getContent());
                });
                adminForm.load({
                    KeyId: KeyId,
                    url: """);
                EndContext();
                BeginContext(4101, 18, false);
#line 100 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
                     Write(Url.Action("Find"));

#line default
#line hidden
                EndContext();
                BeginContext(4119, 392, true);
                WriteLiteral(@""",
                    callBack: function (r) {
                        setTimeout(function () {
                            editor.setContent(r.Member_Introduce ? r.Member_Introduce.replace(/'/g, '\'') : """");
                        }, 300);
                    }
                });
                

            },
            Save: function () {
                var loadUrl = """);
                EndContext();
                BeginContext(4512, 18, false);
#line 111 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
                          Write(Url.Action("Save"));

#line default
#line hidden
                EndContext();
                BeginContext(4530, 919, true);
                WriteLiteral(@""";
                var newData = adminForm.createFormData({
                    _Member_formFile_Photo_One: $(""Input[name=Member_Photo]"")[0].files[0],
                    _Member_formFile__Two: $(""Input[name=Member_FilePath]"")[0].files[0]
                });
             
                adminForm.Save({
                    url: loadUrl,
                    data: newData,
                    isupfile: true
                });
            },
            FindBack: {//查找带回类
                CallBack: function (row, tag, dom) {
                    if (row != null && row.length > 0) row = row[0];
                    if (tag == ""User"") {
                        model.Member_UserID(row ? row._ukid : """");
                        model.User_Name(row ? row.User_Name : """");
                    }
                }
            },
            Delete: function () {
                 adminList.delete('");
                EndContext();
                BeginContext(5450, 20, false);
#line 133 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
                              Write(Url.Action("Delete"));

#line default
#line hidden
                EndContext();
                BeginContext(5470, 240, true);
                WriteLiteral("\', function () {\r\n                     App.Refresh();\r\n                     console.log(\"删除完成！\");\r\n                 });\r\n             },\r\n             //导出\r\n             ExportExecl: function () {\r\n\r\n                 adminList.exportExcel(\"");
                EndContext();
                BeginContext(5711, 25, false);
#line 141 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
                                   Write(Url.Action("ExportExecl"));

#line default
#line hidden
                EndContext();
                BeginContext(5736, 215, true);
                WriteLiteral("\");\r\n             },\r\n             //刷新\r\n             Refresh: function (data) {\r\n\r\n                 adminList.refresh(data);\r\n             },\r\n             Print: function () {\r\n\r\n                 adminList.print(\'");
                EndContext();
                BeginContext(5952, 19, false);
#line 150 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
                             Write(Url.Action("Print"));

#line default
#line hidden
                EndContext();
                BeginContext(5971, 90, true);
                WriteLiteral("\');\r\n            }\r\n          \r\n\r\n        };\r\n\r\n         function vModel() {\r\n            ");
                EndContext();
                BeginContext(6063, 79, false);
#line 157 "G:\AdminCore\core_project_practice\AdminManagement\Areas\Admin\Views\Base\Member\Info.cshtml"
        Write(
                UI.CreateKOViewModel(new MemberM(),"User_Name")
            );

#line default
#line hidden
                EndContext();
                BeginContext(6143, 30, true);
                WriteLiteral("\r\n        }\r\n\r\n    </script>\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
