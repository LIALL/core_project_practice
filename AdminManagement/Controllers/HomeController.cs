﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AdminManagement.Models;
using Microsoft.AspNetCore.Diagnostics;
using System.Text;
namespace AdminManagement.Controllers
{
    using Common;
    using NPOI.SS.Formula.Functions;

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Login", new { Area = "Admin" });
        }
        //异常拦截

        public IActionResult Error() {
           
            var Faeture = HttpContext.Features.Get<IExceptionHandlerFeature>();  //获取信息
            var error = Faeture?.Error;
            var IsAjaxRequest = Tools.IsAjaxRequest;
            if (error is MessageBox)
            {
                //提示的错误
                if (IsAjaxRequest)
                {
                    return Json(MessageBox.errorModel);
                }
                var errorModel = new ErrorModel(error.Message); 
            
                var sb = new StringBuilder();
                sb.Append("< script src =\"/HzyUI/lib/jquery/jquery-2.1.4.min.js\"></script>");
                sb.Append("< script src = \"/ Admin / lib / layer - v3.1.1 / layer / layer.js\"> </script>");

                sb.Append("  < script src = \"/ Admin / js / admin.js\"></ script >");

                sb.Append("  < script  type='Javascript'>");
                var MsgText = errorModel.msg.Trim();
                MsgText = MsgText.Replace("'","“");
                MsgText = MsgText.Replace("\"", "”");
                sb.Append("$function(){ admin.alert('ic" + MsgText + "','警告')}");
                sb.Append("</script>");
                return Content(sb.ToString(), "text/html;charset=utf-8");
            }
            else {

                //系统错误
                Tools.log.WriteLog(error,HttpContext.Connection.RemoteIpAddress.ToString()); //Logn4写入txt
                var errorModel = new ErrorModel(error);
                if (IsAjaxRequest) {

                    return Json(errorModel);
                }
                return View(AppConfig.ErrorPageUrl, errorModel);

            }

          
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

       //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
