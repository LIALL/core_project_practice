﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AdminManagement.NetWork.Controllers
{
    public class HomeController : Controller
    {
        [Area("")]
        public IActionResult Index()
        {
            return View();
        }
    }
}