﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AdminManagement
{
    using System.IO;
    using UEditor.Core;

    public class Startup
    {
        public static log4net.Repository.ILoggerRepository _loggerRepository { get; set; }
       
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _loggerRepository = Common.LogService.Log.CreateRepositiry(_loggerRepository);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;//关闭GDPR规范
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //注册session
            services.AddSession(item =>
            {

                item.IdleTimeout = TimeSpan.FromMinutes(60 * 3);

            });
            //ASP.NET Core中提供了一个IHttpContextAccessor接口，HttpContextAccessor 默认实现了它简化了访问HttpContext。
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //自定义 视图
            services.Configure<Microsoft.AspNetCore.Mvc.Razor.RazorViewEngineOptions>(item =>
            {
                item.AreaViewLocationFormats.Clear();
                item.AreaViewLocationFormats.Add("/Views/Shared/{0}.cshtml");

                item.AreaViewLocationFormats.Add("/Areas/{2}/Views/{1}/{0}.cshtml");
                item.AreaViewLocationFormats.Add("/Areas/{2}/Views/Shared/{0}.cshtml");
                item.AreaViewLocationFormats.Add("/Areas/{2}/Views/Sys/{1}/{0}.cshtml");//系统管理
                item.AreaViewLocationFormats.Add("/Areas/{2}/Views/Base/{1}/{0}.cshtml");//基础信息管理
                item.AreaViewLocationFormats.Add("/Areas/{2}/Views/Operate/{1}/{0}.cshtml");//运营管理
                item.AreaViewLocationFormats.Add("/Areas/{2}/Views/Statistics/{1}/{0}.cshtml");//统计管理
            });

            //注入链接字符串
            DbFrame.DBContext.Initialization(Configuration.GetSection("AppConfig:SqlServerConnStr").Value);

            //Ueditor  编辑器 服务端 注入  configFileRelativePath: "wwwroot/Admin/lib/nUeditor/net/config.json", isCacheConfig: false, basePath: "C:/basepath"
            services.AddUEditorService(configFileRelativePath: Directory.GetCurrentDirectory() + "/wwwroot/Admin/lib/nUeditor/config.json",
                isCacheConfig: false,
                basePath: Directory.GetCurrentDirectory() + "\\wwwroot\\Admin\\lib\\nUeditor\\");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            app.UseExceptionHandler("/Home/Error");  //程序内注册Exception  使用/Home/Error来控制
            app.UseHsts();
            //}

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            //Session注册
            app.UseSession();
            //将 对象 IHttpContextAccessor 注入 HttpContextHelper 静态对象中
            Common.HttpContextService.HttpContextHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());
            app.UseMvc(routes =>
            {
                routes.MapRoute(

                      name: "areas",

                      template: "{area:exists}/{controller=Login}/{action=Index}/{id?}"
                 );
                routes.MapRoute(
                      name: "default",
                      template: "{controller=Home}/{action=Index}/{id?}");
                });
        }
    }
}
