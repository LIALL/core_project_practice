﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.LogService
{
    using log4net.Config;
    using System.IO;
    using log4net.Repository;
    using log4net;

    public class Log
    {
        public ILog _ILog;

        public Log() {

            if (_ILog == null) {

                _ILog = LogManager.GetLogger(AppConfig.LogNETCoreRepositoryName, "\r\n-------------------------程序异常----------------------------\r\n");
            }
                
        }
        /// <summary>
        /// 初始化Log4net 配置
        /// </summary>
        /// <param name="_loggerRepository"></param>
        /// <returns></returns>
        public static ILoggerRepository CreateRepositiry(ILoggerRepository _loggerRepository) {


            _loggerRepository = LogManager.CreateRepository(AppConfig.LogNETCoreRepositoryName);
            XmlConfigurator.Configure(_loggerRepository, new FileInfo("Log4Net/log4net.config")); //加载配置文件
            return _loggerRepository;
        }
        public void WriteLog(string text) {


            _ILog.Error(text); 
        }
        public void WriteLog(Exception _exception, string UserHostAddress, Action<StringBuilder> CallBack = null) {
       
            var sb = new StringBuilder();

            var _Mesage = "异常信息" + _exception.Message;
            var _Source = "错误源" + _exception.Source;
            var _StackTrace = "堆栈信息" + _exception.StackTrace;
            sb.Append("\r\n" + UserHostAddress + "\r\n" + _Mesage + "\r\n" + _Source + "\r\n" + _StackTrace + "\r\n");
            CallBack?.Invoke(sb);
            this.WriteLog(sb.ToString());

        }

    }
}
