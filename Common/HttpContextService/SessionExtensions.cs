﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.HttpContextService
{
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    public static class SessionExtensions
    {
        /// <summary>
        /// 设置VALUE
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <param name=""></param>
        /// <param name="value"></param>
        public static void SetObjectAsJson(this ISession session, string key, object value) {

            session.SetString(key, JsonConvert.SerializeObject(value));
        }
        /// <summary>
        /// 返回VALUE
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetObjectFrom<T>(this ISession session, string key) {

            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
                
        }

    }
}
