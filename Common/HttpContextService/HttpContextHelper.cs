﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
namespace Common.HttpContextService
{
    /// <summary>
    /// HttpContext注入类
    /// </summary>
    public static  class HttpContextHelper
   {
        private static IHttpContextAccessor _Accessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor) {

            _Accessor = httpContextAccessor;
            
        }
        public static HttpContext HttpContext => _Accessor.HttpContext;
    }
}
