﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppHtml.BaseControl
{
   public  class OneLabel :BaseClass
    {
        public OneLabel(string _Name, Dictionary<string, string> _Attribute) {

            this.Name = _Name;
            this.Attribute = _Attribute;
        }
        public string GetHtml() {

            return "<" + this.Name + " " + this.GetAtttibuteString()+" />";
        }

    }
}
