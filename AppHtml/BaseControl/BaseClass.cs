﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppHtml.BaseControl
{
    /// <summary>
    /// 标签库 基础类
    /// </summary>
    public  class BaseClass
    {
          
       public string Name { get; set; }
       public Dictionary<string, string> Attribute { get; set; }

        public string GetAtttibuteString() {

            if (Attribute == null) {

                throw new ArgumentException("标签库 基础类 Attribute不能 为 null");
            }
            var str = "";
            foreach (var item in Attribute)
            {

                str += item.Key + "=\"" + item.Value + "\"";
            }
            return str;
        }

    }
}
