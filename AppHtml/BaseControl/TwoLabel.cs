﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppHtml.BaseControl
{
    public  class TwoLabel :BaseClass
    {
        public string Text { get; set; }
        public TwoLabel(string _Name, Dictionary<string, string> _Attribute, string _Text = "") {

            this.Name = _Name;
            this.Attribute = _Attribute;
            this.Text = _Text;
                
        }
        public string GetHtml() {

            return "<" + this.Name + " " + this.GetAtttibuteString() + ">" + Text + "</" + this.Name + ">";
        }

    }
}
