using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
	//
	using Model;
	using DAL;
	using System.Collections;
	using BLL.Class;
	using Common;
	using DbFrame;
	using DbFrame.Class;

    public class SaleProductBL : BaseBLL
    {
        SaleProductM _SaleProductM = new SaleProductM();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
            return new SaleProductDA().GetDataSource(query, page, rows);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(SaleProductM model)
        {

            if (model.SaleProduct_ID.ToGuid().Equals(Guid.Empty))
            {
                model.SaleProduct_ID = Db.Add(model, li).ToGuid();
                if (model.SaleProduct_ID.Equals(Guid.Empty))
                    throw new MessageBox(Db.ErrorMessage);
            }
            else
            {
                if (!Db.EditById(model, li))
                    throw new MessageBox(Db.ErrorMessage);
            }

            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);

            return model.SaleProduct_ID.ToGuidStr();


        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
		    Db.JsonToList<string>(ID).ForEach(item =>
            {
                if (!Db.DeleteById<SaleProductM>(item.ToGuid(),  li))
                    throw new MessageBox(Db.ErrorMessage);
            });
            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);
            return true;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {

            var _SaleProductM = Db.FindById<SaleProductM>(ID);
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"_SaleProductM",_SaleProductM},
                {"status",1}
            });
            return di;
        }

    }
}
