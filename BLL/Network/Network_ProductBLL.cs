using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
	//
	using Model;
	using DAL;
	using System.Collections;
	using BLL.Class;
	using Common;
	using DbFrame;
	using DbFrame.Class;

    public class Network_ProductBLL : BaseBLL
    {
        Network_ProductM _Network_ProductM = new Network_ProductM();
  
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
            return new Network_ProductDA().GetDataSource(query, page, rows);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(Network_ProductM model)
        {

            if (model.Product_ID.ToGuid().Equals(Guid.Empty))
            {
                model.Product_ID = Db.Add(model, li).ToGuid();
                if (model.Product_ID.Equals(Guid.Empty))
                    throw new MessageBox(Db.ErrorMessage);
            }
            else
            {
                if (!Db.EditById(model, li))
                    throw new MessageBox(Db.ErrorMessage);
            }

            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);

            return model.Product_ID.ToGuidStr();


        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
		    Db.JsonToList<string>(ID).ForEach(item =>
            {
                if (!Db.DeleteById<Network_ProductM>(item.ToGuid(),  li))
                    throw new MessageBox(Db.ErrorMessage);
            });
            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);
            return true;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {

            var _Network_ProductM = Db.FindById<Network_ProductM>(ID);
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"_Network_ProductM",_Network_ProductM},
                {"status",1}
            });
            return di;

          //  return null;
        }

    }
}
