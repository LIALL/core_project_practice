using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
	//
	using Model;
	using DAL;
	using System.Collections;
	using BLL.Class;
	using Common;
	using DbFrame;
	using DbFrame.Class;

    public class Network_HomeBLL : BaseBLL
    {
        Network_HomeM _Network_HomeM = new Network_HomeM();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
            return new Network_HomeDA().GetDataSource(query, page, rows);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(Network_HomeM model)
        {

            if (model.Home_ID.ToGuid().Equals(Guid.Empty))
            {
                model.Home_ID = Db.Add(model, li).ToGuid();
                if (model.Home_ID.Equals(Guid.Empty))
                    throw new MessageBox(Db.ErrorMessage);
            }
            else
            {
                if (!Db.EditById(model, li))
                    throw new MessageBox(Db.ErrorMessage);
            }

            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);

            return model.Home_ID.ToGuidStr();

            //return null;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
		    Db.JsonToList<string>(ID).ForEach(item =>
            {
                if (!Db.DeleteById<Network_HomeM>(item.ToGuid(),  li))
                    throw new MessageBox(Db.ErrorMessage);
            });
            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);
            return true;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<Sys_MenuM> Findtype(string MenuNum)
        {
            //     .WhereIF(!string.IsNullOrEmpty(query["Member_Name"].ToStr()), (a, b) => a.Member_Name.Contains(query["Member_Name"].ToStr()))
            List<Sys_MenuM> _Sys_MenuM = Db.FindList<Sys_MenuM>("select Menu_ID,Menu_Name from  Sys_Menu where Menu_ParentID=( select Menu_ParentID from Sys_Menu where Menu_Num='" + MenuNum + "')", null).ToList() ;
          
            //var _List = Db.Query<Sys_MenuM>((a) => new { a.Menu_Num, a.Menu_Name }).WhereIF(!string.IsNullOrEmpty(MenuNum.ToStr()), (a) => a.Menu_ParentID == );
            //var di = this.EntityToDictionary(new Dictionary<string, object>()
            //  {
            //      {"__Sys_MenuM",_Sys_MenuM}
            //  });
           
            return _Sys_MenuM;

            //return null;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {

            var _Network_HomeM = Db.FindById<Network_HomeM>(ID);
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"_Network_HomeM",_Network_HomeM},
                {"status",1}
            });
            return di;


        }

    }
}
