using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
	//
	using Model;
	using DAL;
	using System.Collections;
	using BLL.Class;
	using Common;
	using DbFrame;
	using DbFrame.Class;

    public class XsDownLoadBLL : BaseBLL
    {
        XsDownLoadM _XsDownLoadM = new XsDownLoadM();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int page, int rows)
        {
            return new XsDownLoadDA().GetDataSource(query, page, rows);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(XsDownLoadM model)
        {
		
			if (model.XsDownLoad_ID.ToGuid().Equals(Guid.Empty))
            {
                model.XsDownLoad_ID = Db.Add(model, li).ToGuid();
                if (model.XsDownLoad_ID.Equals(Guid.Empty))
                    throw new MessageBox(Db.ErrorMessage);
            }
            else
            {
                if (!Db.EditById(model,li))
                    throw new MessageBox(Db.ErrorMessage);
            }

            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);

            return model.XsDownLoad_ID.ToGuidStr();
			
			
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
            Db.JsonToList<string>(ID).ForEach(item =>
            {
                if (!Db.DeleteById<XsDownLoadM>(item.ToGuid(),  li))
                    throw new MessageBox(Db.ErrorMessage);
            });
            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);
            return true;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {

            var _XsDownLoadM = Db.FindById<XsDownLoadM>(ID);
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"_XsDownLoadM",_XsDownLoadM},
                {"status",1}
            });
            return di;

           
        }

    }
}
