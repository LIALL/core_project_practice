﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    using Model;
    using DAL;
    using System.Collections;
    using BLL.Class;
    using Common;
    using DbFrame;
    using DbFrame.Class;
   public class Sys_FunctionBL :BaseBLL
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int pageindex, int pagesize)
        {
            return new Sys_FunctionDA().GetDataSource(query, pageindex, pagesize);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(Sys_FunctionM model)
        {
            if (model.Function_ID.ToGuid().Equals(Guid.Empty))
            {
                model.Function_ID = Db.Add(model, li).ToGuid();
                if (model.Function_ID.ToGuid().Equals(Guid.Empty))
                    throw new MessageBox(Db.ErrorMessage);
            }
            else
            {
                if (!Db.EditById<Sys_FunctionM>(model, li))
                    throw new MessageBox(Db.ErrorMessage);
            }
            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);

            return model.Function_ID.ToGuidStr();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
            Db.JsonToList<string>(ID).ForEach(item =>
            {
                if (!Db.DeleteById<Sys_FunctionM>(item.ToGuid(), li))
                    throw new MessageBox(Db.ErrorMessage);
            });
            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);
            return true;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {
            var functionM = Db.FindById<Sys_FunctionM>(ID.ToGuid());
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"functionM",functionM},
                {"status",1}
            });
            return di;
        }


    }
}
