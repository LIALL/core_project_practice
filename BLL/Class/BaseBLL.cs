﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace BLL.Class
{
    using Model;
    using Common;
    using DbFrame;
    using System.Reflection;
    using System.Data;
    using DbFrame.Class;
    public  class BaseBLL
    {
        protected Sys_AccountM Account = new Sys_AccountM();


        public BaseBLL() {

            Account = this.GetSession<Sys_AccountM>("Account");
        }
        protected List<SQL> li = new List<SQL>();
        public DBContext Db => new DBContext();
        /// <summary>
        /// 设置Session
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetSession(string key, object value)
        {
          
            Tools.SetSession(key, value);
        }
        /// <summary>
        /// 获取设置Session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetSession<T>(string key)
        {
            return Tools.GetSession<T>(key);
        }
        /// <summary>
        /// 将多个实体组合成为一个 字典类型
        /// </summary>
        /// <param name="di"></param>
        /// <returns></returns>
        public Dictionary<string, object> EntityToDictionary(Dictionary<string, object> di)
        {
            Dictionary<string, object> r = new Dictionary<string, object>();
            foreach (var item in di)
            {
                if (item.Value is EntityClass)
                {
                    ReflexHelper.GetPropertyInfos(item.Value.GetType()).ToList().ForEach(pi =>
                    {
                        if (pi.GetValue(item.Value, null) == null)
                            r.Add(pi.Name, null);
                        else
                        {
                            if (pi.PropertyType == typeof(DateTime))
                                r.Add(pi.Name, pi.GetValue(item.Value, null).ToDateTimeFormat("yyyy-MM-dd HH:mm:ss"));
                            else
                                r.Add(pi.Name, pi.GetValue(item.Value, null));
                        }
                    });
                }
                else
                {
                    r.Add(item.Key, item.Value);
                }
            }
            return r;
        }

    }
}
