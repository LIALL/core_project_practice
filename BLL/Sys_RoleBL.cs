﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    using Model;
    using DAL;
    using System.Collections;
    using BLL.Class;
    using Common;
    using DbFrame;
    using DbFrame.Class;
   public class Sys_RoleBL :BaseBLL
    {

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Sys_PagingEntity GetDataSource(Hashtable query, int pageindex, int pagesize)
        {
            return new Sys_RoleDA().GetDataSource(query, pageindex, pagesize);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Save(Sys_RoleM model)
        {
            if (model.Role_ID.ToGuid().Equals(Guid.Empty))
            {
                model.Role_ID = Db.Add(model, li).ToGuid();
                if (model.Role_ID.Equals(Guid.Empty))

                    throw new MessageBox(Db.ErrorMessage);
            }
            else
            {
                if (!Db.EditById(model, li))
                    throw new MessageBox(Db.ErrorMessage);
            }

            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);

            return model.Role_ID.ToGuidStr();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(string ID)
        {
            Db.JsonToList<string>(ID).ForEach(item =>
            {
                var _Sys_RoleM = Db.FindById<Sys_RoleM>(item.ToGuid());
                if (_Sys_RoleM.Role_IsDelete == 2) throw new MessageBox("该信息无法删除！");
                if (!Db.DeleteById<Sys_RoleM>(item.ToGuid(), li))
                    throw new MessageBox(Db.ErrorMessage);
            });
            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);
            return true;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Dictionary<string, object> Find(Guid ID)
        {
            var roleM = Db.FindById<Sys_RoleM>(ID.ToGuid());
            var di = this.EntityToDictionary(new Dictionary<string, object>()
            {
                {"roleM",roleM},
                {"status",1}
            });
            return di;
        }

        /// <summary>
        /// 保存角色功能
        /// </summary>
        public void SaveFunction(string rows, string roleid)
        {
            if (roleid.ToGuid() == Guid.Empty)
                throw new MessageBox("请选择角色");

            if (!Db.Delete<Sys_RoleMenuFunctionM>(w => w.RoleMenuFunction_RoleID == roleid.ToGuid(), li)) //删除角色功能
                throw new MessageBox(Db.ErrorMessage);
            //保存
            Db.JsonToList<Sys_MenuFunctionM>(rows).ForEach(item =>
            {
                Db.Add<Sys_RoleMenuFunctionM>(new Sys_RoleMenuFunctionM
                {
                    RoleMenuFunction_MenuID = item.MenuFunction_MenuID,
                    RoleMenuFunction_FunctionID = item.MenuFunction_FunctionID,
                    RoleMenuFunction_RoleID = roleid.ToGuid()
                }, li);
            });

            if (!Db.Commit(li))
                throw new MessageBox(Db.ErrorMessage);
        }

    }
}
