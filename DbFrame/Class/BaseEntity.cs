﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DbFrame.Class
{
   public class BaseEntity<T> :EntityClass where T:BaseEntity<T>,new()
    {
        //初始化 T 得 FieldInfo 字段信息
        public BaseEntity() {

            this.SetFieldInfo((li) =>
            {
                //实体初始化 将实体信息静态存入当前对象中
                var list = ReflexHelper.GetPropertyInfos(typeof(T));    

                foreach (var item in list) {

                    var _FieldAttribute = (Attribute.GetCustomAttribute(item, typeof(FieldAttribute)) as FieldAttribute);
                    if (_FieldAttribute == null) continue;
                    li.Add(new FieldInfo()
                    {
                        Alias = _FieldAttribute.Alias,
                        FieldName = item.Name,
                        FieldType = item.PropertyType,
                        IsIdentity = _FieldAttribute.IsIdentity,
                        IsIgonre = _FieldAttribute.IsIgnore,
                        IsPrimaryKey=_FieldAttribute.IsPrimaryKey
                    });
                }
            });
                
        }
        /// <summary>
        /// 获取表名
        /// </summary>
        /// <returns></returns>
        public override string GetTableName()
        {

            var type = typeof(T);
            var attr = Attribute.GetCustomAttributes(type, true);

            if (attr.Length == 0) { throw new Exception("实体：" +type.Name+ "未描述实体对象名"); }

            var _TableAttribute = attr.Where(item => item is TableAttribute).FirstOrDefault();

            if (_TableAttribute == null) { return string.Empty; }

            return ((TableAttribute)_TableAttribute).TableName;
        }
        /// <summary>
        /// 获取主键信息
        /// </summary>
        /// <returns></returns>
        public override FieldInfo GetKey() {

            var _FieldInfo = this.GetFieldInfo().Where(w => w.IsPrimaryKey == true).FirstOrDefault();

            var _KeyValue = ReflexHelper.GetPropertyInfos(typeof(T)).Where(w => w.Name == _FieldInfo.FieldName).FirstOrDefault().GetValue(this);


            _FieldInfo.Value = _KeyValue;

            return _FieldInfo;
        }

    }
   
   
    public class FieldInfo {
        /// <summary>
        /// 字段描述
        /// </summary>
        public string Alias = string.Empty;
        /// <summary>
        /// 是否为主键
        /// </summary>
        public bool IsPrimaryKey = false;
        /// <summary>
        /// 是否为自增
        /// </summary>
        public bool IsIdentity = false;
        /// <summary>
        /// 是否为忽略字段（不进行增删改）
        /// </summary>
        public bool IsIgonre = false;
        /// <summary>
        /// 字段类型
        /// </summary>
        public Type FieldType = typeof(Guid);
        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName = string.Empty;
        /// <summary>
        /// 字段值
        /// </summary>
        public object Value = null;
    }
   
}
