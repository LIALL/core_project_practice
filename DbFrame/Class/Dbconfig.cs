﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbFrame.Class
{
    using System.Data;
   public class Dbconfig
    {


        public static string ConnectionString { get; set; }

        public static EDataBaseType _EDataBaseType = EDataBaseType.SqlServer;

        public static Func<string, IDbConnection> GetDbConnection;

        public static Func<string, int, int, object, Paging> FindPaging;

        public static string GetLastInsertId = string.Empty;

        public static Func<string, string, string, object, string> FindMaxNumber;

    }
}
