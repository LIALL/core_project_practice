﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbFrame.Class
{
   public class SQL
    {
        /// <summary>
        /// 未参数化
        /// </summary>
        public string Sql { get; set; }

        /// <summary>
        /// 参数化SQL
        /// </summary>
        public string Sql_Parameter{ get; set; }

        /// <summary>
        /// 参数化 值   
        /// </summary>
        public Dictionary<string, object> Parameter = new Dictionary<string, object>();

        public SQL(string _Sql_Parameter, Dictionary<string, object> _Parameter)
        {
            this.Sql_Parameter = _Sql_Parameter;     //参数化SQL
            this.Parameter = _Parameter;   //Parameter 参数值
            this.Sql = _Sql_Parameter; //未参数化

            foreach (var item in Parameter)
            {
                Sql = Sql.Replace("@" + item.Key, item.Value == null ? null : "'" + item.Value.ToString() + "' ");
            }
        }

    }
}
