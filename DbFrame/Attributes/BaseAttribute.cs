﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbFrame.Class
{
   public class BaseAttribute :Attribute
    {

        public BaseAttribute() { }
        /// <summary>
        /// 错误信息
        /// </summary>

        public string ErrorMessage { get; set; }
    }
}
