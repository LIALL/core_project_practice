﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbFrame.Class
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property,AllowMultiple =false, Inherited =true)]
   public  class FieldAttribute :System.Attribute
    {
        //字段描述
        public string Alias = string.Empty;

        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsPrimaryKey = false;
        /// <summary>
        /// 是否自增
        /// </summary>
        public bool IsIdentity = false;
        /// <summary>
        /// 是否忽略
        /// </summary>
        public bool IsIgnore = false;
        /// <summary>
        /// 属性类型
        /// </summary>
        public Type FieldType = typeof(Guid);
        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName = string.Empty;

        public FieldAttribute(string _ALias) {

            this.Alias = _ALias;
            this.IsPrimaryKey = false;
            this.IsIdentity = false;
            this.IsIgnore = false;
            this.FieldType = null;
            this.FieldName = string.Empty;
        }
    }
}
