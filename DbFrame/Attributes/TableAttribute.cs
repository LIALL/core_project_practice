﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbFrame.Class
{
    public  class TableAttribute:Attribute
    {
        //表名称  自定义属性
        public string TableName = string.Empty;
        public TableAttribute(string _TableName) {

            this.TableName = _TableName;
        }
    }
}
