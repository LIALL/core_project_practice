﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbFrame.SQLContext.Abstract
{


    using System.Linq.Expressions;
    using DbFrame.Class;
    using DbFrame.ExpressionTree;
    public class FindBaseClass :BaseClass
    {
        protected SQL GetSqlStr<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
          where T : BaseEntity<T>, new()
        {
            Code = new StringBuilder();
            var Parma = new Dictionary<string, object>();
            var _Model = (T)Activator.CreateInstance(typeof(T));
            var Alias = new Dictionary<string, string>();

            //Select
            Code.Append(" SELECT ");

            if (Select == null)
            {
                Code.Append("* ");
            }
            else
            {
                Parser.Select(Select, Code, Alias);
            }

            Code.Append(" FROM " + _Model.GetTableName() + " ");

            //Where
            if (Where != null)
            {
                var _ParserArgs = new ParserArgs();

                Code.Append("AS " + Where.Parameters[0].Name + " WHERE 1=1 ");

                Parser.Where(Where, _ParserArgs);
                Code.Append(" AND " + _ParserArgs.Builder);
                foreach (var item in _ParserArgs.SqlParameters)
                {
                    Parma.Add(item.Key, item.Value);
                }
            }

            //OrderBy
            if (OrderBy != null)
            {
                Code.Append(" ORDER BY ");
                Parser.OrderBy(OrderBy, Code, Alias);
            }

            return new SQL(Code.ToString(), Parma);
        }

        protected SQL GetSqlStrById<T>(Expression<Func<T, object>> Select, object Id, Expression<Func<T, object>> OrderBy)
            where T : BaseEntity<T>, new()
        {
            Code = new StringBuilder();
            var Parma = new Dictionary<string, object>();
            var _Model = (T)Activator.CreateInstance(typeof(T));
            var Alias = new Dictionary<string, string>();

            //Select
            Code.Append(" SELECT ");
            if (Select == null)
            {
                Code.Append("* ");
            }
            else
            {
                Parser.Select(Select, Code, Alias);
            }
            Code.Append(" FROM " + _Model.GetTableName() + " ");

            //Where
            var _ParserArgs = new ParserArgs();
            Code.Append("WHERE 1=1 ");
            var KeyName = _Model.GetKey().FieldName;
            Code.Append(" AND " + KeyName + "=@" + KeyName);
            Parma.Add(KeyName, Id);

            //OrderBy
            if (OrderBy != null)
            {
                Code.Append(" ORDER BY ");
                Parser.OrderBy(OrderBy, Code, Alias);
            }

            return new SQL(Code.ToString(), Parma);
        }

        protected SQL GetSqlStr<T>(string Select, Expression<Func<T, bool>> Where)
            where T : BaseEntity<T>, new()
        {
            Code = new StringBuilder();
            var Parma = new Dictionary<string, object>();
            var _Model = (T)Activator.CreateInstance(typeof(T));

            //Select

            Code.Append(" SELECT ");

            if (string.IsNullOrEmpty(Select))
                Code.Append("* ");
            else
                Code.Append(Select);

            Code.Append(" FROM " + _Model.GetTableName() + " ");

            //Where

            if (Where != null)
            {
                var _ParserArgs = new ParserArgs();

                Code.Append("AS " + Where.Parameters[0].Name + " WHERE 1=1 ");

                Parser.Where(Where, _ParserArgs);
                Code.Append(" AND " + _ParserArgs.Builder);
                foreach (var item in _ParserArgs.SqlParameters)
                {
                    Parma.Add(item.Key, item.Value);
                }
            }

            return new SQL(Code.ToString(), Parma);
        }
    }
}
