﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DbFrame.SQLContext.Interface
{
    using DbFrame.Class;
    using System.Linq.Expressions;
    /// <summary>
    /// UPDATE 表名称 SET 列名称 = 新值 WHERE 列名称 = 某值
    /// </summary>
    public interface IEdit
    {
        /// <summary>
        ///    //根据 拉姆达表达式 作为 Where 条件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Where"></param>
        /// <returns></returns>
        bool Edit<T>(T Set, Expression<Func<T, bool>> Where) where T : BaseEntity<T>, new();
        bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where) where T : BaseEntity<T>, new();

    
        bool Edit<T>(T Set, Expression<Func<T, bool>> Where, List<SQL> Li) where T : BaseEntity<T>, new();

        bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity<T>, new();

        //根据ID作为Where 条件

        bool EditById<T>(T Set) where T : BaseEntity<T>, new();


        bool EditById<T>(Expression<Func<T>> Where) where T : BaseEntity<T>, new();

        bool EditById<T>(T Set, List<SQL> Li) where T : BaseEntity<T>, new();

        bool EditById<T>(Expression<Func<T>> Set, List<SQL> li) where T : BaseEntity<T>, new();

        //根据Set 更加灵活操作
        bool EditCustomSet<T>(Expression<Func<T, Object>> Set, Expression<Func<T, bool>> Where) where T : BaseEntity<T>, new();

        bool EditCustomSet<T>(Expression<Func<T, object>> Set, Expression<Func<T, bool>> Where, List<SQL> Li) where T : BaseEntity<T>, new();


    }
}
